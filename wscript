#!/usr/bin/env python

import sys, os 

def options(opt):
    opt.load('compiler_cxx')
    opt.load('compiler_c')

def configure(conf):
    conf.load('compiler_cxx')
    conf.load('compiler_c')
    
    #platform specific
    if sys.platform == 'darwin': 
        conf.env.INCLUDES_OS = ['/opt/local/include',
                                '/opt/local/include/libxml2']
        conf.env.LIBPATH_OS = ['/opt/local/lib']
        conf.env.LIB_OS = ['m', 'xml2', 'fltk', 'fltk_gl', 'portmidi','GLEW']
        conf.env.FRAMEWORK_OS = ['Cocoa','OpenGL', 'AGL', 'Carbon', 
                                 'Accelerate', 'IOKit','System', 'AppKit',
                                 'CoreFoundation']
        conf.env.DEFINES_OS  = ['OSX=1']
        conf.env.DEFINES_GL  = ['GL=1']
    elif sys.platform == 'win32' or sys.platform == 'cygwin':
        conf.env.INCLUDES_OS = ['os/win/include/', 'C:\MinGW\include']
        conf.env.LIBPATH_OS = [os.path.join(os.getcwd(), 'os/win/lib/')]
        conf.env.LIB_OS = ['m', 'ws2_32', 'xml2', 'GLU', 'GL',
                           'fltk', 'fltk_gl','boost_system']
        conf.env.DEFINES_OS  = ['WINDOWS=1']
    else :
        conf.env.INCLUDES_OS = ['/usr/include', '/usr/local/include',
                                '/usr/include/libxml2']
        conf.env.LIB_OS = ['X11', 'm','portmidi','porttime',
                           'xml2', 'pthread','GL','GLEW',
                           'fltk', 'fltk_gl']
        conf.env.LIBPATH_OS = ['/usr/local/lib/']
        conf.env.DEFINES_OS  = ['LINUX=1']
        conf.env.DEFINES_GL  = ['GL=1']
   

    #release specific
    conf.env.CXXFLAGS = ['-O3', '-Wall'] 
    conf.env.DEFINES  = ['DEBUG(x)=//x']

    #debug specific
    conf.setenv('debug', env=conf.env.derive())
    conf.env.CXXFLAGS = ['-g', '-Wall']
    conf.env.DEFINES  = ['DEBUG(x)=std::cout<< x <<std::endl;']

def build(bld):
    macApp = False

    if sys.platform == 'darwin': 
        installPath = '/opt/bin/'
        macApp = 'True'
        osCode = 'src/mac/*.cpp'
    elif sys.platform == 'win32' or sys.platform == 'cygwin':
        osCode = 'src/win/*.cpp'
    else :
        osCode = 'src/x11/*.cpp'

    bld.objects(
          source  = bld.path.ant_glob('src/osc/ip/posix/*.cpp')
                    +bld.path.ant_glob('src/osc/ip/*.cpp')
                    +bld.path.ant_glob('src/osc/osc/*.cpp'),
          use     = ['OS'],
          target  = 'oscpack')

    bld.objects(
        source  = bld.path.ant_glob(osCode),
        use     = ['OS','GL'],
        target  = 'winsman')

    bld.program(
        source       = bld.path.ant_glob('src/*.cpp'),
        use          = ['OS', 'GL', 'winsman', 'oscpack'],
        target       = 'controllar'+bld.variant,
        vnum         = '0.0.1',
        mac_app      = macApp,
        mac_plist     = 'data/Info.plist',
        mac_resources = 'data/controllar.icns',
    )

    bld.install_files('${PREFIX}/share/applications', 
                       ['data/controllar.desktop'])
    bld.install_files('${PREFIX}/share/icons', 
                   ['data/controllar.png'])

from waflib.Build import BuildContext, CleanContext
class debug(BuildContext): 
    cmd = 'debug'
    variant = 'debug' 

