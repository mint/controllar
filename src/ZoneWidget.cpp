/***************************************************************************
 *  ZoneWidget.cpp
 *  Part of 
 *  2016-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "ZoneWidget.hpp"
#include <iostream>
#include <sstream>
#include <cmath>

#include "Controlar.hpp"

using namespace std;

const int ZoneWidget::GLOBAL_PROPS;

ZoneWidget::ZoneWidget(int x, int y, int w, int h): Fl_Widget(x,y,w,h,"") {
    box(FL_FLAT_BOX);
    m_state=NORMAL_STATE;
    m_imageData=NULL;
    m_image=NULL;
    m_coordImage=NULL;
    recreateImage();
    m_dragging=false;
    m_forcedDragging=false;
    m_editing=false;
    m_highlight=false;
    m_scaleX=1;
    m_scaleY=1;
    m_midiLearning=false;
    m_effectDelay.tv_sec=0;
    m_effectDelay.tv_usec=250000;
    m_hiddenByEffect=false;
    m_effectDelayOver=true;
    m_rateReached=false;
    m_initialized=false;
    m_visibleInScene=-1;
    m_updateInScene=-1;
    m_content=CONTENT_GLOBAL;

    m_curCutProps=ZoneWidget::GLOBAL_PROPS;
    m_cutProps[m_curCutProps]=CutProps();
    m_currentScene=0;
}

void ZoneWidget::copy(ZoneWidget* original) {
    resize(original->x(), original->y(), original->w(), original->h());
    m_cutProps=original->m_cutProps;
    m_curCutProps=original->m_curCutProps;
    m_visibleInScene=original->m_visibleInScene;
    m_updateInScene=original->m_updateInScene;
    m_content = original->m_content;
    recomputePixels();
}

ZoneWidget::~ZoneWidget() {}

void ZoneWidget::setScene(const int& scene) {
    if(m_updateInScene==m_currentScene && m_cutProps[m_curCutProps].m_ovWin) {
        m_cutProps[m_curCutProps].m_ovWin->storeImage(m_currentScene);
    }
    if(m_content==CONTENT_LOCAL) {
        m_curCutProps=scene;
    }
    else {
        m_curCutProps=ZoneWidget::GLOBAL_PROPS;
    }
    m_currentScene=scene;
    recomputePixels();
}

void ZoneWidget::setContent(const ZONE_CONTENT& cont) { 
    m_content=cont;
    //set current scene to define current cut props
    setScene(m_currentScene);
    //copy props
    if(m_content==CONTENT_LOCAL) {
        //copy from global to local
        m_cutProps[m_curCutProps]=m_cutProps[ZoneWidget::GLOBAL_PROPS];
    }
    else {
        //copy from local to global
        m_cutProps[ZoneWidget::GLOBAL_PROPS]=m_cutProps[m_curCutProps];
    }
}

void ZoneWidget::refreshCutWin() {
    setCutWin(Controlar::getInstance()
                ->getWindow(m_cutProps[m_curCutProps].m_ovWinName));
}

void ZoneWidget::setCutWin(CutWindow* ow) { 
    CutProps& props = m_cutProps[m_curCutProps];
    props.m_ovWin=ow;
    if(props.m_ovWin) { //if the window exists
        if(props.m_ovWinName.compare(props.m_ovWin->getName())==0) { //same name
            //preserve coordinates if possible
            props.m_winW = max(1, min(props.m_winW,
                              props.m_ovWin->getWidth()));
            props.m_winH = max(1, min(props.m_winH, 
                              props.m_ovWin->getHeight()));
            props.m_winX = max(0, min(props.m_winX, 
                              props.m_ovWin->getWidth()-props.m_winW));
            props.m_winY = max(0, min(props.m_winY, 
                              props.m_ovWin->getHeight()-props.m_winH));
            m_nbPixPerRow=props.m_winW;
        }
        else { //different window
            //reset position and size of the cut
            props.m_ovWinName=props.m_ovWin->getName();
            props.m_winX=0;
            props.m_winY=0;
            props.m_winW = max(1, min(w(), props.m_ovWin->getWidth()));
            props.m_winH = max(1, min(h(), props.m_ovWin->getHeight()));
            m_nbPixPerRow=props.m_winW;
        }
        recomputePixels();
    }
}

CutWindow* ZoneWidget::getCutWin() {
    return m_cutProps[m_curCutProps].m_ovWin;
}

void ZoneWidget::setInputEffect(const ZONE_INPUT_EFFECT& effect) {
    ZONE_INPUT_EFFECT& meffect = m_cutProps[m_curCutProps].m_effect;
    meffect=effect;
    m_hiddenByEffect=(meffect==SHOW)?true:false;
}

void ZoneWidget::setVisible(const int& vis) {
    m_visibleInScene=vis;
}

void ZoneWidget::setUpdate(const int& up) {
    m_updateInScene=up;
}

void ZoneWidget::processMidi(const int& midiType, 
                             const int& midiChannel, 
                             const int& midiControl, 
                             const int& midiValue) {
    if(m_midiLearning) {
        m_midiType=midiType;
        m_midiChannel=midiChannel;
        m_midiControl=midiControl;
        m_midiLearning=false;
    }
    ZONE_INPUT_EFFECT& meffect = m_cutProps[m_curCutProps].m_effect;
    if(meffect!=NO_EFFECT) {
        if(midiType==m_midiType 
                && midiChannel==m_midiChannel 
                && midiControl==m_midiControl) {
            m_effectDelayOver=false;
            gettimeofday(&m_effectStartTime, NULL);
            switch(meffect) {
                case SHOW:{
                    m_hiddenByEffect=false;
                }break;
                case HIDE:{
                    m_hiddenByEffect=true;
                }break;
                default:break;
            }
        }
    }
}

void ZoneWidget::drawZone(const int& currentScene) {
    int nw=w()-OUTER*2;
    int nh=h()-OUTER*2;

    CutProps& props = m_cutProps[m_curCutProps];

    Controlar* cont = Controlar::getInstance();
    //process effect
    if(props.m_effect!=NO_EFFECT && !m_effectDelayOver) {
        timeval curTime, diffTime;
        gettimeofday(&curTime, NULL);
        timersub(&curTime, &m_effectStartTime, &diffTime);
        if(timercmp(&diffTime, &m_effectDelay, >)) {
            m_effectDelayOver=true; 
            switch(props.m_effect) {
                case SHOW:{
                    m_hiddenByEffect=true;
                }break;
                case HIDE:{
                    m_hiddenByEffect=false;
                }break;
                default:break;
            }
        }
    }

    //draw
#ifdef GL
    if(m_visibleInScene<0 || m_visibleInScene==currentScene) {  
        glUniform1f(cont->getZoneFilledUniform(), 0.0);
        glUniform1i(cont->getZoneStateUniform(), int(m_state));
        glUniform2f(cont->getZoneSizeUniform(), w(), h());
        glUniform2f(cont->getZonePosUniform(), x(), y());
        glUniform2f(cont->getCutSizeUniform(), nw, nh);
        glUniform2f(cont->getCutPosUniform(), x()+OUTER, y()+OUTER);
        glUniform1f(cont->getZoneAlphaUniform(), 
                    m_hiddenByEffect?0.0:float(props.m_alpha)/255.0);
        glUniform1f(cont->getZoneInvertUniform(), props.m_color==INVERT);
        if(props.m_ovWin) {
            glUniform1f(cont->getZoneFilledUniform(), 1.0);
            //if using stored image from other scene
            if(m_updateInScene>=0 && m_updateInScene!=currentScene) {
                uchar* storedData=props.m_ovWin->retrieveImage(m_updateInScene);
                if(storedData) {
                    glActiveTexture(GL_TEXTURE0);
                    glBindTexture(GL_TEXTURE_2D, cont->getFirstTextureID());
                    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
                                 m_nbPixPerRow, props.m_ovWin->getHeight(),
                                 0, GL_RGBA, GL_UNSIGNED_BYTE,
                                 storedData);
                    cont->setCurrentWindowName("");
                }
            }
            else if(cont->getCurrentWindowName()
                                .compare(props.m_ovWin->getName())!=0) {
                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, cont->getFirstTextureID());
                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
                             m_nbPixPerRow, props.m_ovWin->getHeight(),
                             0, GL_RGBA, GL_UNSIGNED_BYTE,
                             props.m_ovWin->grabImage());
                cont->setCurrentWindowName(props.m_ovWin->getName());
            }
            glActiveTexture(GL_TEXTURE1);
            glBindTexture(GL_TEXTURE_2D, cont->getSecondTextureID());
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RG16F,
                         nw, nh,
                         0, GL_RG, GL_FLOAT,
                         m_coordImage);
        }
        glDrawArrays(GL_TRIANGLES, 0, 6);
    }

#else 
    
    //process drawing
    if(props.m_ovWin) {
        if(m_visibleInScene<0 || m_visibleInScene==currentScene) {  
            if(!m_hiddenByEffect && m_rateReached) {
                m_image->uncache();
                m_ovWin->getPixels(props.m_winX, props.m_winY, 
                                   props.m_winW, props.m_winH, 
                                   m_srcIndices, m_srcCoords, m_destIndices, 
                                   props.m_alpha, props.m_color, m_imageData);
                m_image->draw(x(), cont->isFlipped()?cont->h()-y()-h():y(), 
                                  w(), h());
            }
            switch(m_rate) {
                case STATIC:m_rateReached=false;break;
                default:m_rateReached=true;break;
            }
        }
    }
    else {
        fl_rect(x(), cont->isFlipped()?cont->h()-y()-h():y(), 
                w(), h(), FL_WHITE);
    }
    if(m_editing) {
        fl_rectf(x(), cont->isFlipped()?cont->h()-y()-h():y(), 
                 w(), h(), FL_RED);
    }
    if(m_highlight) {
        fl_rect(x(), cont->isFlipped()?cont->h()-y()-h():y(), 
                w(), h(), FL_RED);
    }

#endif
}

void ZoneWidget::recomputePixels() {
    CutProps& props = m_cutProps[m_curCutProps];

    //if window attached, recompute correspondance between pixel coordinates 
    if(props.m_ovWin) {

        //get new pixperrow in window
        m_nbPixPerRow = props.m_ovWin->computeNbPixPerRow(props.m_winW, 
                                                          props.m_winH);

        //clear existing coordinates 
        m_srcIndices.clear();
        m_srcCoords.clear();
        m_destIndices.clear();
        vector<float> coord(2,0);
        vector<float> start(2,0);
        vector<bool> restart(2,false);
        vector<float> stepY(2,0);
        vector<float> stepX(2,0);
        int nw=w()-OUTER*2;
        int nh=h()-OUTER*2;

        //compute the pixels offset and steps depending on transformation
        switch(props.m_transfo) {
            case ROTATE_90 : {
                start[0]=props.m_ovWin->needsOffset()?props.m_winX:0;
                start[1]=props.m_ovWin->needsOffset()?props.m_winY+props.m_winH
                                                     :props.m_winH;
                stepY[0]=float(props.m_winW)/float(nh);
                restart[1]=true;
                stepX[1]=-float(props.m_winH)/float(nw);
            }break;
            case ROTATE_180 : {
                start[0]=props.m_ovWin->needsOffset()?props.m_winX+props.m_winW
                                                     :props.m_winW;
                start[1]=props.m_ovWin->needsOffset()?props.m_winY+props.m_winH
                                                     :props.m_winH;
                stepY[1]=-float(props.m_winH)/float(nh);
                restart[0]=true;
                stepX[0]=-float(props.m_winW)/float(nw);
            }break;
            case ROTATE_270 : {
                start[0]=props.m_ovWin->needsOffset()?props.m_winX+props.m_winW
                                                     :props.m_winW;
                start[1]=props.m_ovWin->needsOffset()?props.m_winY:0;
                stepY[0]=-float(props.m_winW)/float(nh);
                restart[1]=true;
                stepX[1]=float(props.m_winH)/float(nw);
            }break;
            default : {
                start[0]=props.m_ovWin->needsOffset()?props.m_winX:0;
                start[1]=props.m_ovWin->needsOffset()?props.m_winY:0;
                stepY[1]=float(props.m_winH)/float(nh);
                restart[0]=true;
                stepX[0]=float(props.m_winW)/float(nw);
            }break;
        }

        vector<int> intCoord(2,0);
        coord[0]=start[0];
        coord[1]=start[1];

        Controlar* cont = Controlar::getInstance();

        //compute coordinates according to shape
        switch(props.m_shape) { 
            case CIRCLE : { //circle shape
                int radius = min(nw,nh);
                for(int py = 0; py < nh; py++) {
                    for(int px = 0; px < nw; px++) {
                        float distX = px-nw/2;
                        float distY = py-nh/2;
                        float dist = sqrt(distX*distX + distY*distY);
                        if(dist>radius/4 && dist<radius/2) { //inside the circle
                            distX/=float(radius);
                            distY/=float(radius);
                            float angleY = (atan2(distY,distX)-M_PI/2.0)
                                            / (2.0*M_PI)*float(nh);
                            if(angleY<0) {
                                angleY=nh+angleY;
                            }
                            float radiusX = 
                                (dist-radius/4)/(radius/4)*float(nw);
                            intCoord[0] = 
                                start[0]+stepY[0]*angleY+stepX[0]*radiusX;
                            intCoord[1] = 
                                start[1]+stepY[1]*angleY+stepX[1]*radiusX;
                        }
                        else {
                            intCoord[0]=-1; intCoord[1]=-1; 
                        }
                        m_srcCoords.push_back(intCoord);
                        int srcInd = (m_nbPixPerRow*intCoord[1]
                                        +intCoord[0])*4;
                        if(props.m_ovWin->isBGR()) {
                            m_srcIndices.push_back(srcInd+2);
                            m_srcIndices.push_back(srcInd+1);
                            m_srcIndices.push_back(srcInd);
                        }
                        else {
                            m_srcIndices.push_back(srcInd);
                            m_srcIndices.push_back(srcInd+1);
                            m_srcIndices.push_back(srcInd+2);
                        }
                        m_srcIndices.push_back(srcInd+3);
                        for(int c=0; c<4; ++c) {
                            if(cont->isFlipped()) {
                               m_destIndices.push_back((nw*(nh-1-py)+px)*4+c);
                            }
                            else {
                                m_destIndices.push_back((nw*py+px)*4+c);
                            }
                        }
                        m_coordImage[(nw*py+px)*2]=float(intCoord[0]) 
                                            / float(props.m_ovWin->getWidth());
                        m_coordImage[(nw*py+px)*2+1]=float(intCoord[1]) 
                                            / float(props.m_ovWin->getHeight());
                    }
                }
            }break;
            case FRAME : { //frame shape
                int border=20;
                for(int py = 0; py < nh; py++) {
                    coord[0]=restart[0]?start[0]:coord[0];
                    coord[1]=restart[1]?start[1]:coord[1];
                    for(int px = 0; px < nw; px++) {
                        if(px>border && px<nw-border
                                && py>border && py<nh-border) {
                            intCoord[0]=-1; intCoord[1]=-1; 
                        }
                        else {
                            float nbSrcPix=0;
                            if(py<border) {
                                nbSrcPix = float(py)/float(border)*float(nh/2);
                                intCoord[0]=coord[0];
                                intCoord[1]=start[1]
                                            +nbSrcPix*stepX[1]
                                            +nbSrcPix*stepY[1];
                            }
                            else if(py>nh-border) {
                                nbSrcPix= float(nh)
                                          -(float(nh-py)
                                              /float(border)
                                              *float(nh/2));
                                intCoord[0]=coord[0];
                                intCoord[1]=start[1]
                                            +nbSrcPix*stepX[1]
                                            +nbSrcPix*stepY[1];
                            }
                            else if(px<border) {
                                nbSrcPix = float(px)/float(border)*float(nw/2);
                                intCoord[0]=start[0]
                                            +nbSrcPix*stepX[0]
                                            +nbSrcPix*stepY[0];
                                intCoord[1]=coord[1];
                            }
                            else {
                                nbSrcPix= float(nw)
                                          -(float(nw-px)
                                              /float(border)
                                              *float(nw/2));
                                intCoord[0]=start[0]
                                            +nbSrcPix*stepX[0]
                                            +nbSrcPix*stepY[0];
                                intCoord[1]=coord[1];
                            }
                        }
                        m_srcCoords.push_back(intCoord);
                        int srcInd = (m_nbPixPerRow*intCoord[1]
                                        +intCoord[0])*4;
                        if(props.m_ovWin->isBGR()) {
                            m_srcIndices.push_back(srcInd+2);
                            m_srcIndices.push_back(srcInd+1);
                            m_srcIndices.push_back(srcInd);
                        }
                        else {
                            m_srcIndices.push_back(srcInd);
                            m_srcIndices.push_back(srcInd+1);
                            m_srcIndices.push_back(srcInd+2);
                        }
                        m_srcIndices.push_back(srcInd+3);
                        for(int c=0; c<4; ++c) {
                            if(cont->isFlipped()) {
                               m_destIndices.push_back((nw*(nh-1-py)+px)*4+c);
                            }
                            else {
                                m_destIndices.push_back((nw*py+px)*4+c);
                            }
                        }
                        coord[0]+=stepX[0];
                        coord[1]+=stepX[1];
                        m_coordImage[(nw*py+px)*2]=float(intCoord[0]) 
                                          / float(props.m_ovWin->getWidth());
                        m_coordImage[(nw*py+px)*2+1]=float(intCoord[1]) 
                                          / float(props.m_ovWin->getHeight());
                    }
                    coord[0]+=stepY[0];
                    coord[1]+=stepY[1];
                }
            }break;
            default: { //box shape
                for(int py = 0; py < nh; py++) {
                    coord[0]=restart[0]?start[0]:coord[0];
                    coord[1]=restart[1]?start[1]:coord[1];
                    for(int px = 0; px < nw; px++) {
                        intCoord[0]=coord[0]; intCoord[1]=coord[1];
                        m_srcCoords.push_back(intCoord);
                        int srcInd = (m_nbPixPerRow*intCoord[1]
                                        +intCoord[0])*4;
                        if(props.m_ovWin->isBGR()) {
                            m_srcIndices.push_back(srcInd+2);
                            m_srcIndices.push_back(srcInd+1);
                            m_srcIndices.push_back(srcInd);
                        }
                        else {
                            m_srcIndices.push_back(srcInd);
                            m_srcIndices.push_back(srcInd+1);
                            m_srcIndices.push_back(srcInd+2);
                        }
                        m_srcIndices.push_back(srcInd+3);
                        for(int c=0; c<4; ++c) {
                            if(cont->isFlipped()) {
                               m_destIndices.push_back((nw*(nh-1-py)+px)*4+c);
                            }
                            else {
                                m_destIndices.push_back((nw*py+px)*4+c);
                            }
                        }
                        coord[0]+=stepX[0];
                        coord[1]+=stepX[1];
                        m_coordImage[(nw*py+px)*2]=float(intCoord[0]) 
                                         / float(props.m_ovWin->getWidth());
                        m_coordImage[(nw*py+px)*2+1]=float(intCoord[1]) 
                                         / float(props.m_ovWin->getHeight());
                    }
                    coord[0]+=stepY[0];
                    coord[1]+=stepY[1];
                }
            }break;
        }
        m_scaleX=(stepY[0]!=0)?abs(stepY[0]):abs(stepX[0]);
        m_scaleY=(stepY[1]!=0)?abs(stepY[1]):abs(stepX[1]);
    }
}

void ZoneWidget::recreateImage() {
    int nw=w()-OUTER*2;
    int nh=h()-OUTER*2;
    if(m_imageData) {
        delete [] m_imageData;
    }
    m_imageData = new uchar[nw*nh*4];
    if(m_image) {
        delete m_image;
    }
    m_image = new Fl_RGB_Image(m_imageData, nw, nh, 4);

    if(m_coordImage) {
        delete m_coordImage;
    }
    m_coordImage = new float[nw*nh*2];
}

int ZoneWidget::handle(int event) {
    CutProps& props = m_cutProps[m_curCutProps];
    int res=0;
    switch(event) {
        case FL_MOUSEWHEEL : {
            Fl_Widget* wid = Fl::belowmouse();
            if(parent()!=NULL) {
                parent()->insert(*wid,0);
            }
            res=1;
        }break;
        case FL_PUSH:{
            if(Fl::event_inside(this)) { 
                m_forcedDragging=false;
                switch(Fl::event_button()) {
                    case FL_RIGHT_MOUSE: {
                        do_callback();
                        res=1;
                    }break;
                    case FL_LEFT_MOUSE: {
                        //if(Fl::event_shift()) { //move/resize zone
                        if(m_state==MOVE_ZONE || m_state==RESIZE_ZONE) { 
                            startDraggingZone();
                            m_editing=true;
                        }
                        //else if(props.m_ovWin) { //move/resize window
                        else if(props.m_ovWin) { //move/resize window
                            m_editing=true;
                            switch(props.m_transfo) {
                                case ROTATE_90: {
                                    m_dragStartX=props.m_winY;
                                    m_dragStartY=props.m_winX;
                                    m_dragX=Fl::event_x();
                                    m_dragY=Fl::event_y();
                                    m_dragW=props.m_winW+Fl::event_y();
                                    m_dragH=props.m_winH-Fl::event_x();
                                }break;
                                case ROTATE_180: {
                                    m_dragStartX=props.m_winX;
                                    m_dragStartY=props.m_winY;
                                    m_dragX=Fl::event_x();
                                    m_dragY=Fl::event_y();
                                    m_dragW=props.m_winW-Fl::event_x();
                                    m_dragH=props.m_winH-Fl::event_y();
                                }break;
                                case ROTATE_270: {
                                    m_dragStartX=props.m_winY;
                                    m_dragStartY=props.m_winX;
                                    m_dragX=Fl::event_x();
                                    m_dragY=Fl::event_y();
                                    m_dragW=props.m_winW-Fl::event_y();
                                    m_dragH=props.m_winH+Fl::event_x();
                                }break;
                                default: {
                                    m_dragStartX=props.m_winX;
                                    m_dragStartY=props.m_winY;
                                    m_dragX=Fl::event_x();
                                    m_dragY=Fl::event_y();
                                    m_dragW=props.m_winW+Fl::event_x();
                                    m_dragH=props.m_winH+Fl::event_y();
                                }break;
                            }
                        }
                        m_dragging=true;
                        res=1;
                    }break;
                    default:break;
                }
            }
        }break;
        case FL_DRAG: {
            if(m_dragging) {
                if(Fl::event_button()==FL_LEFT_MOUSE) {
/*
                    if(Fl::event_shift()) { //move/resize zone
                        m_editing=true;
                        if(Fl::event_command()) { 
                            size(m_dragW+Fl::event_x(), m_dragH+Fl::event_y());
                        }
                        else {
                            position(m_dragX+Fl::event_x(), 
                                     m_dragY+Fl::event_y());
                        }
                    }
*/
                    if(m_state==MOVE_ZONE) {
                        m_editing=true;
                        position(m_dragX+Fl::event_x(), 
                                 m_dragY+Fl::event_y());
                    }
                    else if(m_state==RESIZE_ZONE) {
                        m_editing=true;
                        size(max(float(OUTER*2), m_dragW+Fl::event_x()), 
                             max(float(OUTER*2), m_dragH+Fl::event_y()));
                    }
                    else if(props.m_ovWin) { //move/resize window
                        int transDW, transDH, transDX, transDY;
                        switch(props.m_transfo) {
                            case ROTATE_90: {
                                transDW=m_dragW-Fl::event_y();
                                transDH=m_dragH+Fl::event_x();
                                transDX=m_dragStartY
                                        - m_scaleX*(Fl::event_y()-m_dragY);
                                transDY=m_dragStartX
                                        + m_scaleY*(Fl::event_x()-m_dragX);
                            }break;
                            case ROTATE_180: {
                                transDW=m_dragW+Fl::event_x();
                                transDH=m_dragH+Fl::event_y();
                                transDX=m_dragStartX
                                        + m_scaleX*(Fl::event_x()-m_dragX);
                                transDY=m_dragStartY
                                        + m_scaleY*(Fl::event_y()-m_dragY);
                            }break;
                            case ROTATE_270: {
                                transDW=m_dragW+Fl::event_y();
                                transDH=m_dragH-Fl::event_x();
                                transDX=m_dragStartY
                                        + m_scaleX*(Fl::event_y()-m_dragY);
                                transDY=m_dragStartX
                                        - m_scaleY*(Fl::event_x()-m_dragX);
                            }break;
                            default: {
                                transDW=m_dragW-Fl::event_x();
                                transDH=m_dragH-Fl::event_y();
                                transDX=m_dragStartX
                                        - m_scaleX*(Fl::event_x()-m_dragX);
                                transDY=m_dragStartY
                                        - m_scaleY*(Fl::event_y()-m_dragY);
                            }break;
                        }
                        //if(Fl::event_command()) {
                        if(m_state==RESIZE_CUT) {
                            props.m_winW = max(1, min(transDW,
                                            props.m_ovWin->getWidth()));
                            props.m_winH = max(1, min(transDH, 
                                            props.m_ovWin->getHeight()));
                            props.m_winX = max(0, min(props.m_winX, 
                                            props.m_ovWin->getWidth()
                                                - props.m_winW));
                            props.m_winY = max(0, min(props.m_winY, 
                                            props.m_ovWin->getHeight() 
                                                - props.m_winH));
                        }
                        else {
                            props.m_winX = max(0, min(transDX, 
                                            props.m_ovWin->getWidth()
                                                - props.m_winW));
                            props.m_winY = max(0, min(transDY, 
                                            props.m_ovWin->getHeight()
                                                - props.m_winH));
                        }
                        recomputePixels();
                    }
                    res=1;
                }
            }
        }break;
        case FL_RELEASE: {
            m_editing=false;
            res=1;
        }break;
        case FL_SHORTCUT: {
            if(m_highlight && Fl::event_key()==FL_Shift_L) {
                m_editing=true;
                return 1;
            }
            else  {
                switch(Fl::event_key()) {
                    case 'd': {
                        m_forcedDragging=false;
                        Controlar::getInstance()->startDuplicating(this);
                        res=1;
                    }break;
                    case 'a': {
                        props.m_alpha=(props.m_alpha<255)?props.m_alpha+10:255;
                        res=1;
                    }break;
                    case 'q': {
                        props.m_alpha=(props.m_alpha>0)?props.m_alpha-10:0;
                        res=1;
                    }break;
                    case 's' : {
                        props.m_shape = ZONE_SHAPE((props.m_shape+1)%NB_SHAPES);
                        recomputePixels();
                        res=1;
                    }break;
                    case 't' : {
                        props.m_transfo 
                            = ZONE_TRANSFO((props.m_transfo+1)%NB_TRANSFO);
                        recomputePixels();
                        res=1;
                    }break;
                    case 'c' : {
                        props.m_color= ZONE_COLOR((props.m_color+1)%NB_COLORS);
                        res=1;
                    }break;
                    case 'e' : {
                        setInputEffect(ZONE_INPUT_EFFECT((props.m_effect+1)
                                        %NB_EFFECTS));
                        res=1;
                    }break;
                    case FL_Left : 
                    case FL_Right : 
                    case FL_Down :
                    case FL_Up : {
                        int transDX=props.m_winX;
                        int transDY=props.m_winY;
                        int transDW=props.m_ovWin->getWidth();
                        int transDH=props.m_ovWin->getHeight();
                        if((Fl::event_key()==FL_Left && props.m_transfo==NONE) 
                                || (Fl::event_key()==FL_Right 
                                        && props.m_transfo==ROTATE_180) 
                                || (Fl::event_key()==FL_Down
                                        && props.m_transfo==ROTATE_270) 
                                || (Fl::event_key()==FL_Up
                                        && props.m_transfo==ROTATE_90) ) {
                            transDX-=10*m_scaleX;
                            transDW-=10*m_scaleX;
                        }
                        else if((Fl::event_key()==FL_Right 
                                    && props.m_transfo==NONE)
                                || (Fl::event_key()==FL_Left 
                                        && props.m_transfo==ROTATE_180) 
                                || (Fl::event_key()==FL_Up
                                        && props.m_transfo==ROTATE_270) 
                                || (Fl::event_key()==FL_Down
                                        && props.m_transfo==ROTATE_90) ) {
                            transDX+=10*m_scaleX;
                            transDW+=10*m_scaleX;
                        }
                        else if((Fl::event_key()==FL_Down 
                                    && props.m_transfo==NONE) 
                                || (Fl::event_key()==FL_Up 
                                        && props.m_transfo==ROTATE_180) 
                                || (Fl::event_key()==FL_Right
                                        && props.m_transfo==ROTATE_270) 
                                || (Fl::event_key()==FL_Left
                                        && props.m_transfo==ROTATE_90) ) {
                            transDY+=10*m_scaleY;
                            transDH+=10*m_scaleY;
                        }
                        else if((Fl::event_key()==FL_Up 
                                    && props.m_transfo==NONE) 
                                || (Fl::event_key()==FL_Down
                                        && props.m_transfo==ROTATE_180) 
                                || (Fl::event_key()==FL_Left
                                        && props.m_transfo==ROTATE_270) 
                                || (Fl::event_key()==FL_Right
                                        && props.m_transfo==ROTATE_90) ) {
                            transDY-=10*m_scaleY;
                            transDH-=10*m_scaleY;
                        }
                        if(Fl::event_command()) {
                            props.m_winW = max(1, min(transDW,
                                                props.m_ovWin->getWidth()));
                            props.m_winH = max(1, min(transDH, 
                                                props.m_ovWin->getHeight()));
                            props.m_winX = max(0, min(props.m_winX, 
                                                props.m_ovWin->getWidth()
                                                - props.m_winW));
                            props.m_winY = max(0, min(props.m_winY, 
                                                props.m_ovWin->getHeight()
                                                - props.m_winH));
                        }
                        else {
                            props.m_winX = max(0, min(transDX, 
                                                props.m_ovWin->getWidth()
                                                - props.m_winW));
                            props.m_winY = max(0, min(transDY, 
                                                props.m_ovWin->getHeight()
                                                - props.m_winH));
                        }
                        recomputePixels();
                        res=1;
                    }break;
                }
            }
        }break;
        case FL_KEYUP: {
            if(m_highlight && Fl::event_key()==FL_Shift_L) {
                m_editing=false;
                res=1;
            }
        }break;
        case FL_MOVE:{
            if(!m_editing) {
                int inPosX = Fl::event_x()-x();
                int inPosY = Fl::event_y()-y();
                if(inPosX>OUTER && inPosY>OUTER 
                        && inPosX<w()-OUTER && inPosY<h()-OUTER) {
                    if(sqrt(pow(inPosX-w(),2)+pow(inPosY-h(),2))<OUTER*3) {
                        m_state=RESIZE_CUT;
                    }
                    else {
                        m_state=MOVE_CUT;
                    }
                }
                else {
                    if(sqrt(pow(inPosX-w(),2)+pow(inPosY-h(),2))<OUTER) {
                        m_state=RESIZE_ZONE;
                    }
                    else {
                        m_state=MOVE_ZONE;
                    }
                }
            }
            if(m_forcedDragging) {
                position(m_dragX+Fl::event_x(), 
                         m_dragY+Fl::event_y());
            }
            res=1;
        }break;
        case FL_ENTER: {
            m_highlight=true;
            m_editing=false;
            res=1;
        }break;
        case FL_LEAVE: {
            m_state=NORMAL_STATE;
            m_highlight=false;
            m_editing=false;
            res=1;
        }break;
        default:break;
    }
    if(res==1) {
        parent()->redraw();
    }
    return res;
}

void ZoneWidget::forceDraggingZone() {
    startDraggingZone();
    m_forcedDragging=true;
}

void ZoneWidget::startDraggingZone() {
    m_dragX=x()-Fl::event_x();
    m_dragY=y()-Fl::event_y();
    m_dragW=w()-Fl::event_x();
    m_dragH=h()-Fl::event_y();
}

void ZoneWidget::resize(int nx, int ny, int nw, int nh) {
    bool recImg=false;
    if(nw!=w() || nh!=h()) {
        recImg=true;
    }
    Fl_Widget::resize(nx, ny, nw, nh);
    if(recImg) {
        recreateImage();
        recomputePixels();
    }
}

void ZoneWidget::load(xmlNodePtr zoneNode) {
    CutProps& props = m_cutProps[m_curCutProps];

    char* value = NULL;

    value = (char*)xmlGetProp(zoneNode,(xmlChar*)"win_name");
    if(value!=NULL) {
        props.m_ovWinName = string(value);
        props.m_ovWin = Controlar::getInstance()->getWindow(props.m_ovWinName);
    }

    value = (char*)xmlGetProp(zoneNode,(xmlChar*)"win_x");
    if(value!=NULL) {
        props.m_winX = atoi(value);
    }
    value = (char*)xmlGetProp(zoneNode,(xmlChar*)"win_y");
    if(value!=NULL) {
        props.m_winY = atoi(value);
    }
    value = (char*)xmlGetProp(zoneNode,(xmlChar*)"win_w");
    if(value!=NULL) {
        props.m_winW = atoi(value);
    }
    value = (char*)xmlGetProp(zoneNode,(xmlChar*)"win_h");
    if(value!=NULL) {
        props.m_winH = atoi(value);
    }
    int x=0,y=0,w=20,h=20;
    value = (char*)xmlGetProp(zoneNode,(xmlChar*)"zone_x");
    if(value!=NULL) {
        x = atoi(value);
    }
    value = (char*)xmlGetProp(zoneNode,(xmlChar*)"zone_y");
    if(value!=NULL) {
        y = atoi(value);
    }
    value = (char*)xmlGetProp(zoneNode,(xmlChar*)"zone_w");
    if(value!=NULL) {
        w = atoi(value);
    }
    value = (char*)xmlGetProp(zoneNode,(xmlChar*)"zone_h");
    if(value!=NULL) {
        h = atoi(value);
    }
    value = (char*)xmlGetProp(zoneNode,(xmlChar*)"alpha");
    if(value!=NULL) {
        props.m_alpha = atoi(value);
    }
    value = (char*)xmlGetProp(zoneNode,(xmlChar*)"transform");
    if(value!=NULL) {
        props.m_transfo = ZONE_TRANSFO(atoi(value));
    }
    value = (char*)xmlGetProp(zoneNode,(xmlChar*)"shape");
    if(value!=NULL) {
        props.m_shape = ZONE_SHAPE(atoi(value));
    }
    value = (char*)xmlGetProp(zoneNode,(xmlChar*)"effect");
    if(value!=NULL) {
        setInputEffect(ZONE_INPUT_EFFECT(atoi(value)));
    }
    value = (char*)xmlGetProp(zoneNode,(xmlChar*)"color");
    if(value!=NULL) {
        setColor(ZONE_COLOR(atoi(value)));
    }
    value = (char*)xmlGetProp(zoneNode,(xmlChar*)"midi_type");
    if(value!=NULL) {
        m_midiType=atoi(value);
    }
    value = (char*)xmlGetProp(zoneNode,(xmlChar*)"midi_channel");
    if(value!=NULL) {
        m_midiChannel=atoi(value);
    }
    value = (char*)xmlGetProp(zoneNode,(xmlChar*)"midi_control");
    if(value!=NULL) {
        m_midiControl=atoi(value);
    }
    value = (char*)xmlGetProp(zoneNode,(xmlChar*)"visible");
    if(value!=NULL) {
        m_visibleInScene=atoi(value);
    }
    value = (char*)xmlGetProp(zoneNode,(xmlChar*)"update");
    if(value!=NULL) {
        m_updateInScene=atoi(value);
    }
    value = (char*)xmlGetProp(zoneNode,(xmlChar*)"content");
    if(value!=NULL) {
        m_content=ZONE_CONTENT(atoi(value));
    }
    resize(x,y,w,h);
}

void ZoneWidget::save(xmlNodePtr parentNode) {
    CutProps& props = m_cutProps[m_curCutProps];
    xmlNodePtr newNode = xmlNewChild(parentNode, NULL, 
                                      BAD_CAST "Zone", NULL);
    if(props.m_ovWin) {
        xmlNewProp(newNode, BAD_CAST "win_name", 
                   BAD_CAST props.m_ovWinName.c_str());
    }
    ostringstream oss1, oss2, oss3, oss4, oss5, 
                  oss6, oss7, oss8, oss9, oss10, 
                  oss11, oss12, oss13, oss14, oss15,
                  oss16, oss17, oss18, oss19;
    oss1<<props.m_winX;
    xmlNewProp(newNode, BAD_CAST "win_x", 
               BAD_CAST oss1.str().c_str());
    oss2<<props.m_winY;
    xmlNewProp(newNode, BAD_CAST "win_y", 
               BAD_CAST oss2.str().c_str());
    oss3<<props.m_winW;
    xmlNewProp(newNode, BAD_CAST "win_w", 
               BAD_CAST oss3.str().c_str());
    oss4<<props.m_winH;
    xmlNewProp(newNode, BAD_CAST "win_h", 
               BAD_CAST oss4.str().c_str());
    oss5<<x();
    xmlNewProp(newNode, BAD_CAST "zone_x", 
               BAD_CAST oss5.str().c_str());
    oss6<<y();
    xmlNewProp(newNode, BAD_CAST "zone_y", 
               BAD_CAST oss6.str().c_str());
    oss7<<w();
    xmlNewProp(newNode, BAD_CAST "zone_w", 
               BAD_CAST oss7.str().c_str());
    oss8<<h();
    xmlNewProp(newNode, BAD_CAST "zone_h", 
               BAD_CAST oss8.str().c_str());
    oss9<<props.m_alpha;
    xmlNewProp(newNode, BAD_CAST "alpha", 
               BAD_CAST oss9.str().c_str());
    oss10<<props.m_transfo;
    xmlNewProp(newNode, BAD_CAST "transform", 
               BAD_CAST oss10.str().c_str());
    oss11<<props.m_shape;
    xmlNewProp(newNode, BAD_CAST "shape", 
               BAD_CAST oss11.str().c_str());
    oss12<<props.m_effect;
    xmlNewProp(newNode, BAD_CAST "effect", 
               BAD_CAST oss12.str().c_str());
    oss13<<m_midiType;
    xmlNewProp(newNode, BAD_CAST "midi_type", 
               BAD_CAST oss13.str().c_str());
    oss14<<m_midiChannel;
    xmlNewProp(newNode, BAD_CAST "midi_channel", 
               BAD_CAST oss14.str().c_str());
    oss15<<m_midiControl;
    xmlNewProp(newNode, BAD_CAST "midi_control", 
               BAD_CAST oss15.str().c_str());
    oss16<<props.m_color;
    xmlNewProp(newNode, BAD_CAST "color", 
               BAD_CAST oss16.str().c_str());
    oss17<<m_visibleInScene;
    xmlNewProp(newNode, BAD_CAST "visible", 
               BAD_CAST oss17.str().c_str());
    oss18<<m_updateInScene;
    xmlNewProp(newNode, BAD_CAST "update", 
               BAD_CAST oss18.str().c_str());
    oss19<<m_content;
    xmlNewProp(newNode, BAD_CAST "content", 
               BAD_CAST oss19.str().c_str());
}


