/***************************************************************************
 *  FlipGroup.hpp
 *  Part of ControllAR
 *  2016-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef FlipGroup_h
#define FlipGroup_h

#include <string>
#include <map>
#include <vector>

#include <FL/Fl_Pack.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Image_Surface.H>

class FlipGroup: public Fl_Group {
    public :
        FlipGroup(int x, int y, int w, int h);
        void draw();
        int handle(int event);
        void addWid(Fl_Widget*);
        void resize(int x, int y, int w, int h);
        void refresh();

    protected:
        Fl_RGB_Image* m_flipImg;
        bool m_wasChanged;
};

#endif
