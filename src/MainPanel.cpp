/***************************************************************************
 *  MainPanel.cpp
 *  Part of ControllAR
 *  2016-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "MainPanel.hpp"
#include <FL/Fl_Button.H>
#include <FL/fl_draw.H>
#include <algorithm>
#include <iostream>
#include "Controlar.hpp"

using namespace std;

MainPanel::MainPanel(): FlipGroup(0, 0, 240, 160) {
    int labW=60;
    int butW=40;
    int smButW=20;
    int butH=20;
    int sep=5;
    Fl_Pack* fileP = new Fl_Pack(sep, sep, (butW+sep)*3, butH, "File");
    fileP->type(Fl_Pack::HORIZONTAL);
    fileP->align(FL_ALIGN_LEFT | FL_ALIGN_INSIDE);
    fileP->spacing(sep);
    fileP->box(FL_NO_BOX);
    add(fileP);
    fileP->add(new Fl_Group(0,0,labW,0,""));
    Fl_Button* opBut = new Fl_Button(0, 0, butW, butH, "open");
    opBut->callback(statOpen,this);
    fileP->add(opBut);
    Fl_Button* saBut = new Fl_Button(0, 0, butW, butH, "save");
    saBut->callback(statSave,this);
    fileP->add(saBut);
    Fl_Button* quBut = new Fl_Button(0, 0, butW, butH, "quit");
    quBut->callback(statQuit,this);
    fileP->add(quBut);

    Fl_Pack* disP = new Fl_Pack(sep, sep+(butH+sep), 
                                (butW+sep)*3, butH, "Display");
    disP->type(Fl_Pack::HORIZONTAL);
    disP->align(FL_ALIGN_LEFT | FL_ALIGN_INSIDE);
    disP->spacing(sep);
    disP->box(FL_NO_BOX);
    add(disP);
    disP->add(new Fl_Group(0,0,labW,0,""));
    Fl_Button* fsBut = new Fl_Button(0, 0, butW*2, butH, "fullscreen");
    fsBut->callback(statFullscreen,this);
    disP->add(fsBut);
    Fl_Button* mirBut = new Fl_Button(0, 0, butW*2, butH, "mirror");
    mirBut->callback(statMirror,this);
    disP->add(mirBut);

    int brW=160;
    int brH=70;
    Fl_Group* inps = new Fl_Group(sep,sep+(butH+sep)*2,brW+labW, brH, "Inputs");
    inps->box(FL_NO_BOX);
    inps->align(FL_ALIGN_TOP|FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
    add(inps);
    m_midiBr = new Fl_Multi_Browser(sep*2+labW, sep+(butH+sep)*2, brW, brH, "");
    m_midiBr->align(FL_ALIGN_TOP);
    m_midiBr->has_scrollbar(Fl_Browser_::VERTICAL_ALWAYS);
    m_midiBr->callback(statMidiBr, this);
    inps->add(m_midiBr);

    Fl_Pack* scnP = new Fl_Pack(sep, sep+(butH+sep)*2+brH+sep, 
                                  brW, butH, "Scenes");
    scnP->align(FL_ALIGN_TOP|FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
    scnP->type(Fl_Pack::HORIZONTAL);
    scnP->spacing(sep);
    scnP->box(FL_NO_BOX);
    add(scnP);
    scnP->add(new Fl_Group(0, 0, labW, 0, ""));
    Fl_Button* prevSc = new Fl_Button(0, 0, smButW, butH, "<");
    prevSc->callback(statScenePrev, this);
    scnP->add(prevSc);
    m_sceneOut = new Fl_Value_Output(0, 0, butW, butH, "");
    m_sceneOut->value(0);
    scnP->add(m_sceneOut);
    Fl_Button* nextSc = new Fl_Button(0, 0, smButW, butH, ">");
    nextSc->callback(statSceneNext, this);
    scnP->add(nextSc);
    Fl_Button* delSc = new Fl_Button(0, 0, smButW, butH, "x");
    delSc->callback(statSceneDel, this);
    scnP->add(delSc);
    Fl_Button* learnSc = new Fl_Button(0, 0, butW, butH, "learn");
    learnSc->callback(statSceneLearn, this);
    scnP->add(learnSc);
}

MainPanel* MainPanel::getInstance() {
    static MainPanel instance;
    return &instance;
}


void MainPanel::cbMidiBr() {
    for(int i=1; i<=m_midiBr->size(); ++i) {
        if(m_midiBr->selected(i)) {
            Controlar::getInstance()->openMidiDevice(m_midiBr->text(i));
        }
        else {
            Controlar::getInstance()->closeMidiDevice(m_midiBr->text(i));
        }
    }
}

void MainPanel::setMidiDevices(const vector<string>& devs, 
                               const vector<bool>& opened) {
    m_midiBr->clear();
    vector<string>::const_iterator itDev = devs.begin();
    vector<bool>::const_iterator itOp = opened.begin();
    int el=1;
    for(; itDev!=devs.end(); ++itDev, ++itOp, ++el) {
        m_midiBr->add((*itDev).c_str(), NULL);
        if((*itOp)) {
            m_midiBr->select(el);
        }
    }
}


