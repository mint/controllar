/***************************************************************************
 *  FlipGroup.cpp
 *  Part of 
 *  2016-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "FlipGroup.hpp"
#include <FL/Fl_Button.H>
#include <FL/fl_draw.H>
#include <algorithm>
#include <iostream>
#include "Controlar.hpp"

using namespace std;

FlipGroup::FlipGroup(int x, int y, int w, int h): Fl_Group(x, y, w, h, "") {
    end();
    box(FL_FLAT_BOX);
    m_wasChanged=true;
}

void FlipGroup::draw() {
    Controlar* cont = Controlar::getInstance();
    if(m_wasChanged) {
        Fl_Image_Surface* surf = new Fl_Image_Surface(w(), h());
        surf->set_current();
        fl_draw_box(box(), 0, 0, 
                    w(), h(), color());
        for(int c=0; c<children(); ++c) {
            surf->draw(child(c), 
                       child(c)->x()-x(), 
                       child(c)->y()-y());
        }
        m_flipImg = surf->image();
        delete surf;
        Fl_Display_Device::display_device()->set_current();
        m_wasChanged=false;
    }
#ifdef GL
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, cont->getFirstTextureID());
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB,
                 w(), h(),
                 0, GL_RGB, GL_UNSIGNED_BYTE,
                 m_flipImg->array);
    glUniform2f(cont->getMenuPosUniform(), x(), y());
    glUniform2f(cont->getMenuSizeUniform(), w(), h());
    glDrawArrays(GL_TRIANGLES, 0, 6);
#else 
    fl_draw_image(statDrawFlipImg, this, 
                  x(), cont->isFlipped()?cont->h()-y()-h():y(), 
                  w(), h(), m_flipImg->d());
#endif
}

int FlipGroup::handle(int event) {
    int res=Fl_Group::handle(event);
    if(res) {
        refresh();
    }
    return 1;
}

void FlipGroup::resize(int x, int y, int w, int h) {
    Fl_Group::resize(x,y,w,h);
    m_wasChanged=true;
}

void FlipGroup::refresh() {
    m_wasChanged=true;
    redraw();
}

void FlipGroup::addWid(Fl_Widget* wid) {
    m_wasChanged=true;
    add(wid);
    wid->box(FL_BORDER_BOX);
    wid->visible_focus(0);
}

