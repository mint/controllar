/***************************************************************************
 *  ZonePanel.hpp
 *  Part of ControllAR
 *  2016-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef ZonePanel_h
#define ZonePanel_h

#include <string>
#include <map>
#include <vector>

#include <FL/Fl_Choice.H>
#include <FL/Fl_Hold_Browser.H>
#include <FL/Fl_Radio_Round_Button.H>
#include <FL/Fl_Radio_Button.H>
#include <FL/Fl_Slider.H>
#include "Controlar.hpp"
#include "FlipGroup.hpp"
#include "CutWindow.hpp"

class ZoneWidget;

class ZonePanel: public FlipGroup {
    public :
        static ZonePanel* getInstance();
        ~ZonePanel(){};
        void setWindowList(std::vector<CutWindow*>& wins);

        static void statWinBr(Fl_Widget* w, void* f){ 
            ZonePanel *tmpf = static_cast<ZonePanel *>(f);
            tmpf->cbWinBr(w);
        }    
        void cbWinBr(Fl_Widget*);

        static void statZoneTransfo(Fl_Widget* w, void* f){ 
            ZonePanel::getInstance()
                ->cbZoneTransfo(*(ZoneWidget::ZONE_TRANSFO*)(f));
        }    
        void cbZoneTransfo(const ZoneWidget::ZONE_TRANSFO&);

        static void statZoneShape(Fl_Widget* w, void* f){ 
            ZonePanel::getInstance()
                ->cbZoneShape(*(ZoneWidget::ZONE_SHAPE*)(f));
        }    
        void cbZoneShape(const ZoneWidget::ZONE_SHAPE&);

        static void statZoneAlpha(Fl_Widget* w, void* f){ 
            ZonePanel::getInstance()->cbZoneAlpha();
        }    
        void cbZoneAlpha();

        static void statZoneColor(Fl_Widget* w, void* f){ 
            ZonePanel::getInstance()
                        ->cbZoneColor(*(ZoneWidget::ZONE_COLOR*)(f));
        }    
        void cbZoneColor(const ZoneWidget::ZONE_COLOR&);

        static void statZoneInput(Fl_Widget* w, void* f){ 
            ZonePanel::getInstance()
                        ->cbZoneInput(*(ZoneWidget::ZONE_INPUT_EFFECT*)(f));
        }    
        void cbZoneInput(const ZoneWidget::ZONE_INPUT_EFFECT&);

        static void statZoneContent(Fl_Widget* w, void* f){ 
            ZonePanel::getInstance()
                        ->cbZoneContent(*(ZoneWidget::ZONE_CONTENT*)(f));
        }    
        void cbZoneContent(const ZoneWidget::ZONE_CONTENT&);

        static void statZoneUpdate(Fl_Widget* w, void* f){ 
            ZonePanel::getInstance()
                        ->cbZoneUpdate(*(ZoneWidget::ZONE_UPDATE*)(f));
        }    
        void cbZoneUpdate(const ZoneWidget::ZONE_UPDATE&);

        static void statZoneLearn(Fl_Widget* w, void* f){ 
            Controlar::getInstance()->cbZoneMidiLearn();
        }    

        static void statZoneDelete(Fl_Widget* w, void* f){ 
            Controlar::getInstance()->cbZoneDelete();
        }    
            
        void setZone(ZoneWidget*);

    private:
        ZonePanel();
        Fl_Hold_Browser* m_winsBr;
        ZoneWidget* m_curZone;
        ZoneWidget::ZONE_TRANSFO m_transfos[ZoneWidget::NB_TRANSFO];
        Fl_Radio_Button* m_transfoButs[ZoneWidget::NB_TRANSFO];
        ZoneWidget::ZONE_SHAPE m_shapes[ZoneWidget::NB_SHAPES];
        Fl_Radio_Button* m_shapeButs[ZoneWidget::NB_SHAPES];
        ZoneWidget::ZONE_COLOR m_colors[ZoneWidget::NB_COLORS];
        Fl_Radio_Button* m_colorButs[ZoneWidget::NB_COLORS];
        ZoneWidget::ZONE_INPUT_EFFECT m_inputs[ZoneWidget::NB_EFFECTS];
        Fl_Radio_Button* m_inputButs[ZoneWidget::NB_EFFECTS];
        ZoneWidget::ZONE_CONTENT m_contents[ZoneWidget::NB_CONTENTS];
        Fl_Radio_Button* m_contentButs[ZoneWidget::NB_CONTENTS];
        ZoneWidget::ZONE_UPDATE m_updates[ZoneWidget::NB_UPDATES];
        Fl_Radio_Button* m_updateButs[ZoneWidget::NB_UPDATES];
        Fl_Slider* m_alphaSlider;
};

#endif
