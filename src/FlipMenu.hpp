/***************************************************************************
 *  FlipMenu.hpp
 *  Part of 
 *  2016-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef FlipMenu_h
#define FlipMenu_h

#include <string>
#include <map>
#include <vector>

#include <FL/Fl_Pack.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Image_Surface.H>
#include <map>
#include <string>

class FlipItemMenu;
class FlipItem;

class FlipMenu: public Fl_Pack {
    public :
        FlipMenu();
        void draw();
        void addWithSub(const std::string& itemName, Fl_Callback*, void* p);
        void add(const std::string& itemName, Fl_Callback*, void* p);
        void addSub(const std::string& subName);
        FlipMenu* getSub(const std::string& subName);
        void hide();
        void clear();
        void hideSubMenus();

    private:
        std::map<std::string, FlipMenu*> m_subMenuMap;

};

class FlipItemMenu : public Fl_Button {
    public :
        FlipItemMenu(FlipMenu* menu, FlipMenu* subMenu);
        int handle(int event);
        void draw();
        void resize(int x, int y, int w, int h);
        static void statDrawFlipImg(void *data,int x,int y, int w, uchar *buf) {
            FlipItemMenu *item = static_cast<FlipItemMenu*>(data);
            item->drawFlipImg(x, y, w, buf);
        }
        void drawFlipImg(int x, int y, int w, uchar* buf);

    private:
        FlipMenu* m_menu;
        FlipMenu* m_subMenu;
        Fl_RGB_Image* m_flipImg;
        Fl_RGB_Image* m_flipImgH;
        bool m_wasResized;
        bool m_highlight;
};

class FlipItem : public Fl_Button {
    public :
        FlipItem(FlipMenu*);
        int handle(int event);
        void draw();
        void resize(int x, int y, int w, int h);
        static void statDrawFlipImg(void *data,int x,int y, int w, uchar *buf) {
            FlipItem *item = static_cast<FlipItem*>(data);
            item->drawFlipImg(x, y, w, buf);
        }
        void drawFlipImg(int x, int y, int w, uchar* buf);

    private:
        FlipMenu* m_menu;
        Fl_RGB_Image* m_flipImg;
        Fl_RGB_Image* m_flipImgH;
        bool m_wasResized;
        bool m_highlight;
};

#endif

