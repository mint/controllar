/***************************************************************************
 *  CutWindow.hpp
 *  Part of Controlar
 *  2016-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef CutWindow_h
#define CutWindow_h

#include <vector>
#include <map>
#include <string>

#include <FL/Fl.H>
#include <FL/Fl_RGB_Image.H>

class CutWindowsManager;
#include "ZoneWidget.hpp"

class CutWindow {
    public :
        CutWindow(CutWindowsManager* man):m_winsMan(man){}
        virtual ~CutWindow(){}

        void setName(const std::string& name){m_name=name;}
        const std::string& getName(){return m_name;}
        virtual void getPixels(const int& x, const int& y, 
                               const int& sx, const int& sy,
                               const std::vector<int>& srcIndices,
                               const std::vector<std::vector<int> >& srcCoords,
                               const std::vector<int>& destIndices,
                               const uchar& alpha,
                               const ZoneWidget::ZONE_COLOR& color,
                               uchar* destImg)=0;
        
        virtual int computeNbPixPerRow(const int& srcW, const int& srcH)=0;
        inline const int& getWidth(){return m_width;}
        inline const int& getHeight(){return m_height;}
        inline const int& getOffsetX(){return m_offsetX;}
        inline const int& getOffsetY(){return m_offsetY;}
        inline const int& getPixPerRow(){return m_pixPerRow;}
        inline bool isBGR(){return m_isBGR;}
        inline bool needsOffset(){return m_needsOffset;}
        inline void setWinID(const int& id){m_winID=id;}
        inline const int& getWinID(){return m_winID;}
    
        virtual uchar* grabImage()=0;
        virtual void releaseImage()=0;

        virtual void storeImage(const int& id)=0;
        virtual uchar* retrieveImage(const int& id)=0;

    protected:
        int m_winID;
        std::string m_name;
        int m_posX, m_posY, m_width, m_height;
        int m_offsetX, m_offsetY, m_pixPerRow;
        bool m_isBGR;
        bool m_needsOffset;
        CutWindowsManager* m_winsMan;
        bool m_grabbed;
        uchar* m_imgData;
        uchar* m_defaultImgData;

        std::map<int, uchar*> m_storedImgData;
};

#endif

