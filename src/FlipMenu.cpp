/***************************************************************************
 *  FlipMenu.cpp
 *  Part of 
 *  2016-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "FlipMenu.hpp"
#include <FL/Fl_Button.H>
#include <FL/fl_draw.H>
#include <algorithm>
#include <iostream>
#include "Controlar.hpp"

using namespace std;

FlipMenu::FlipMenu():Fl_Pack(0, 0, 100, 100, "") {
    type(Fl_Pack::VERTICAL);
    end();
}

void FlipMenu::addWithSub(const std::string& itemName,Fl_Callback* cb,void* p) {
    //parse submenu, create flip menu if needed  
    size_t pos = itemName.find_first_of('/');
    if(pos==string::npos) {
        add(itemName, cb, p);
    }
    else {
        string subName = itemName.substr(0, pos);
        addSub(subName);
        string subItemName = itemName.substr(pos+1);
        m_subMenuMap[subName]->add(subItemName, cb, p);
    }
}

void FlipMenu::add(const std::string& itemName, Fl_Callback* cb, void* p) {
    FlipItem* but = new FlipItem(this);
    but->copy_label(itemName.c_str());
    but->callback(cb, p);
    Fl_Pack::add(but);
    resize(0, 0, 
           max(w(), 
                int(float(itemName.length()*FL_NORMAL_SIZE)/4.0)*4), 
                children()*20);
}

void FlipMenu::addSub(const string& subName) {
    if(m_subMenuMap.find(subName)==m_subMenuMap.end()) {
        m_subMenuMap[subName] = new FlipMenu();
        FlipItemMenu* men = new FlipItemMenu(this, m_subMenuMap[subName]);
        men->copy_label(subName.c_str());
        Fl_Pack::add(men);
    }
}

FlipMenu* FlipMenu::getSub(const string& subName) {
    FlipMenu* res=NULL;
    if(m_subMenuMap.find(subName)!=m_subMenuMap.end()) {
        res=m_subMenuMap[subName];
    }
    return res;
}

void FlipMenu::clear() {
    map<string, FlipMenu*>::iterator itSub = m_subMenuMap.begin();
    for(; itSub!=m_subMenuMap.end(); ++itSub) {
        itSub->second->clear();
        Controlar::getInstance()->remove(itSub->second);
        Fl::delete_widget(itSub->second);
    }
    m_subMenuMap.clear();
    Fl_Pack::clear();
    resize(x(), y(), 100, 100);
}

void FlipMenu::hide() {
    Fl_Pack::hide();
    hideSubMenus();
}

void FlipMenu::draw() {
    Fl_Pack::draw();
}

void FlipMenu::hideSubMenus() {
    map<string, FlipMenu*>::iterator itSub = m_subMenuMap.begin();
    for(; itSub!=m_subMenuMap.end(); ++itSub) {
        itSub->second->hide();
    }
}


//--------------------------------ITEM MENU
FlipItemMenu::FlipItemMenu(FlipMenu* men, FlipMenu* subMen):
                                          Fl_Button(0,0,20,20,""), 
                                          m_menu(men),
                                          m_subMenu(subMen) {
    align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
    box(FL_THIN_DOWN_BOX);
    visible_focus(false);
    m_wasResized=true;
    m_highlight=false;
}

int FlipItemMenu::handle(int event) {
    switch(event) {
        case FL_ENTER: { 
            m_menu->hideSubMenus();
            color(FL_SELECTION_COLOR);
            m_subMenu->position(x()+w(), y());
            Controlar* cont = Controlar::getInstance();
            cont->insert(*m_subMenu, cont->children());
            m_subMenu->show();
            m_highlight=true;
            return 1;
        }break;
        case FL_LEAVE: { 
            color(FL_BACKGROUND_COLOR);
            m_highlight=false;
            return 1; 
        }break;
        default:break;
    }
    return 0;
}

void FlipItemMenu::resize(int x, int y, int w, int h) {
    Fl_Button::resize(x,y,w,h);
    m_wasResized=true;
}

void FlipItemMenu::draw() {
    Controlar* cont = Controlar::getInstance();
    if(m_wasResized) {
        Fl_Image_Surface* surf = new Fl_Image_Surface(w(), h());
        surf->set_current();
        fl_draw_box(box(), 0, 0, 
                    w(), h(), color());
        fl_color(FL_BLACK);
        fl_draw(label(), 
                0, 0, 
                w(), h(), 
                align(), NULL, 0);
        m_flipImg = surf->image();
        delete surf;

        Fl_Display_Device::display_device()->set_current();
        surf = new Fl_Image_Surface(w(), h());
        surf->set_current();
        fl_draw_box(box(), 0, 0, 
                    w(), h(), FL_SELECTION_COLOR);
        fl_color(FL_BLACK);
        fl_draw(label(), 
                0, 0, 
                w(), h(), 
                align(), NULL, 0);
        m_flipImgH = surf->image();
        delete surf;

        Fl_Display_Device::display_device()->set_current();
        m_wasResized=false;
    }

#ifdef GL
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, cont->getFirstTextureID());
    if(m_highlight) {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB,
                     w(), h(),
                     0, GL_RGB, GL_UNSIGNED_BYTE,
                     m_flipImgH->array);
    }
    else {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB,
                     w(), h(),
                     0, GL_RGB, GL_UNSIGNED_BYTE,
                     m_flipImg->array);
    }
    glUniform2f(cont->getMenuPosUniform(), x(), y());
    glUniform2f(cont->getMenuSizeUniform(), w(), h());
    glDrawArrays(GL_TRIANGLES, 0, 6);
#else 
    if(m_highlight) {
        fl_draw_image(statDrawFlipImg, this, 
                      x(), cont->isFlipped()?cont->h()-y()-h():y(), 
                      w(), h(), m_flipImgH->d());
    }
    else {
        fl_draw_image(statDrawFlipImg, this, 
                      x(), cont->isFlipped()?cont->h()-y()-h():y(), 
                      w(), h(), m_flipImg->d());
    }
#endif
    if(m_subMenu->visible()) {
        m_subMenu->draw();
    }
}

void FlipItemMenu::drawFlipImg(int x, int y, int w, uchar* buf) {
    if(Controlar::getInstance()->isFlipped()) {
        if(m_highlight) {
            memcpy(buf, 
                   &m_flipImgH->array[((m_flipImgH->h()-y)*m_flipImgH->w()+x)
                        *m_flipImgH->d()], 
                   w*m_flipImgH->d());
        }
        else {
            memcpy(buf, 
                   &m_flipImg->array[((m_flipImg->h()-y)*m_flipImg->w()+x)
                        *m_flipImg->d()], 
                   w*m_flipImg->d());
        }
    }
    else {
        if(m_highlight) {
            memcpy(buf, 
                   &m_flipImgH->array[(y*m_flipImgH->w()+x)*m_flipImgH->d()], 
                   w*m_flipImgH->d());
        }
        else {
            memcpy(buf, 
                   &m_flipImg->array[(y*m_flipImg->w()+x)*m_flipImg->d()], 
                   w*m_flipImg->d());
        }
    }
}

//-------------------------------ITEM 
FlipItem::FlipItem(FlipMenu* men):Fl_Button(0,0,20,20,""),
                                  m_menu(men) {
    align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
    box(FL_THIN_UP_BOX);
    visible_focus(false);
    m_wasResized=true;
    m_highlight=false;
}

int FlipItem::handle(int event) {
    switch(event) {
        case FL_ENTER: { 
            m_menu->hideSubMenus();
            color(FL_SELECTION_COLOR);
            m_highlight=true;
            return 1;
        }break;
        case FL_LEAVE: { 
            color(FL_BACKGROUND_COLOR);
            m_highlight=false;
            return 1; 
        }break;
        case FL_PUSH: {
            if(Fl::event_button()==FL_LEFT_MOUSE) {
                do_callback();
                color(FL_BACKGROUND_COLOR);
                m_menu->hide();
                return 1;
            }
        }break;
        default:break;
    }
    return 0;
}

void FlipItem::resize(int x, int y, int w, int h) {
    Fl_Button::resize(x,y,w,h);
    m_wasResized=true;
}

void FlipItem::draw() {

    Controlar* cont = Controlar::getInstance();
    if(m_wasResized) {
        Fl_Image_Surface* surf = new Fl_Image_Surface(w(), h());
        surf->set_current();
        fl_draw_box(box(), 0, 0, 
                    w(), h(), color());
        fl_color(FL_BLACK);
        fl_draw(label(), 
                0, 0, 
                w(), h(), 
                align(), NULL, 0);
        m_flipImg = surf->image();
        delete surf;

        Fl_Display_Device::display_device()->set_current();
        surf = new Fl_Image_Surface(w(), h());
        surf->set_current();
        fl_draw_box(box(), 0, 0, 
                    w(), h(), FL_SELECTION_COLOR);
        fl_color(FL_BLACK);
        fl_draw(label(), 
                0, 0, 
                w(), h(), 
                align(), NULL, 0);
        m_flipImgH = surf->image();

        delete surf;
        Fl_Display_Device::display_device()->set_current();
        m_wasResized=false;
    }

#ifdef GL
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, cont->getFirstTextureID());
    if(m_highlight) {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB,
                     w(), h(),
                     0, GL_RGB, GL_UNSIGNED_BYTE,
                     m_flipImgH->array);
    }
    else {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB,
                     w(), h(),
                     0, GL_RGB, GL_UNSIGNED_BYTE,
                     m_flipImg->array);
    }
    glUniform2f(cont->getMenuPosUniform(), x(), y());
    glUniform2f(cont->getMenuSizeUniform(), w(), h());
    glDrawArrays(GL_TRIANGLES, 0, 6);
#else 
    if(m_highlight) {
        fl_draw_image(statDrawFlipImg, this, 
                      x(), cont->isFlipped()?cont->h()-y()-h():y(), 
                      w(), h(), m_flipImgH->d());
    }
    else {
        fl_draw_image(statDrawFlipImg, this, 
                      x(), cont->isFlipped()?cont->h()-y()-h():y(), 
                      w(), h(), m_flipImg->d());
    }
#endif
}

void FlipItem::drawFlipImg(int x, int y, int w, uchar* buf) {
    if(Controlar::getInstance()->isFlipped()) {
        if(m_highlight) {
            memcpy(buf, 
                   &m_flipImgH->array[((m_flipImgH->h()-y)*m_flipImgH->w()+x)
                        *m_flipImgH->d()], 
                   w*m_flipImgH->d());
        }
        else {
            memcpy(buf, 
                   &m_flipImg->array[((m_flipImg->h()-y)*m_flipImg->w()+x)
                        *m_flipImg->d()], 
                   w*m_flipImg->d());
        }
    }
    else {
        if(m_highlight) {
            memcpy(buf, 
                   &m_flipImgH->array[(y*m_flipImgH->w()+x)*m_flipImgH->d()], 
                   w*m_flipImgH->d());
        }
        else {
            memcpy(buf, 
                   &m_flipImg->array[(y*m_flipImg->w()+x)*m_flipImg->d()], 
                   w*m_flipImg->d());
        }
    }
}

