/***************************************************************************
 *  MainPanel.hpp
 *  Part of ControllAR
 *  2016-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef MainPanel_h
#define MainPanel_h

#include <FL/Fl_Multi_Browser.H>
#include <FL/Fl_Value_Output.H>

#include "Controlar.hpp"
#include "FlipGroup.hpp"

class MainPanel: public FlipGroup {
    public :
        static MainPanel* getInstance();
        ~MainPanel(){};
        static void statOpen(Fl_Widget* w,void* f){ 
            Controlar::getInstance()->cbOpen();
        }    
        static void statSave(Fl_Widget* w, void* f){ 
            Controlar::getInstance()->cbSave();
        }    
        static void statQuit(Fl_Widget* w,void* f){ 
            Controlar::getInstance()->cbQuit();
        }    

        static void statMidiBr(Fl_Widget* w, void* f){ 
            MainPanel::getInstance()->cbMidiBr();
        }    
        void cbMidiBr();
        static void statSceneNext(Fl_Widget* w, void* f){ 
            Controlar::getInstance()->cbSceneNext();
            MainPanel::getInstance()->refresh();
        }    
        static void statScenePrev(Fl_Widget* w, void* f){ 
            Controlar::getInstance()->cbScenePrev();
            MainPanel::getInstance()->refresh();
        }    
        static void statSceneDel(Fl_Widget* w,void* f){ 
            Controlar::getInstance()->cbSceneDelete();
            MainPanel::getInstance()->refresh();
        }    
        static void statSceneLearn(Fl_Widget* w,void* f){ 
            Controlar::getInstance()->cbSceneLearn();
            MainPanel::getInstance()->refresh();
        }    
        static void statFullscreen(Fl_Widget* w,void* f){ 
            Controlar::getInstance()->cbFullscreen();
            MainPanel::getInstance()->refresh();
        }    
        static void statMirror(Fl_Widget* w,void* f){ 
            Controlar::getInstance()->cbFlip();
            MainPanel::getInstance()->refresh();
        }    
        void setMidiDevices(const std::vector<std::string>& midiDeviceNames,
                            const std::vector<bool>& midiDeviceOp);
        inline void setCurrentScene(const int& sc){m_sceneOut->value(sc);}

    private:
        MainPanel();
        Fl_Multi_Browser* m_midiBr;
        Fl_Value_Output* m_sceneOut;
};

#endif
