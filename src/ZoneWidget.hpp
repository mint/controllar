/***************************************************************************
 *  ZoneWidget.hpp
 *  Part of 
 *  2016-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef ZoneWidget_h
#define ZoneWidget_h

#include <FL/Fl.H>
#include <FL/Fl_Widget.H>
#include <FL/fl_draw.H>
#include <GL/glew.h>
#include <FL/gl.h>

#include <libxml/parser.h>
#include <libxml/tree.h>

#include <sys/time.h>
#include <string>
#include <vector>
#include <map>

class CutWindow;

class ZoneWidget : public Fl_Widget {

    public:
        enum ZONE_TRANSFO{NONE, ROTATE_90, ROTATE_180, ROTATE_270, NB_TRANSFO};
        enum ZONE_SHAPE{BOX, CIRCLE, FRAME, NB_SHAPES};
        enum ZONE_INPUT_EFFECT{NO_EFFECT, SHOW, HIDE, NB_EFFECTS};
        enum ZONE_COLOR{NORMAL, INVERT, NB_COLORS};
        enum ZONE_VISIBLE{VISIBLE_ALL, VISIBLE_CURRENT};
        enum ZONE_UPDATE{UPDATE_ALL, UPDATE_CURRENT, NB_UPDATES};
        enum ZONE_CONTENT{CONTENT_GLOBAL, CONTENT_LOCAL, NB_CONTENTS};
        enum ZONE_STATE{NORMAL_STATE,MOVE_CUT,RESIZE_CUT,MOVE_ZONE,RESIZE_ZONE};

        static const int INNER=20;
        static const int OUTER=20;

    struct CutProps {
        CutProps(): m_ovWin(NULL), m_ovWinName(""), m_winX(0), m_winY(0), 
                    m_winW(0), m_winH(0), m_alpha(255), m_transfo(NONE), 
                    m_color(NORMAL), m_shape(BOX), m_effect(NO_EFFECT){}
        CutWindow* m_ovWin;
        std::string m_ovWinName;
        int m_winX, m_winY;
        int m_winW, m_winH;
        int m_alpha;
        ZONE_TRANSFO m_transfo;
        ZONE_COLOR m_color;
        ZONE_SHAPE m_shape;
        ZONE_INPUT_EFFECT m_effect;
    };

    public:
        ZoneWidget(int x=0, int y=0, int w=10, int h=10);
        ~ZoneWidget();
        void copy(ZoneWidget*);
        inline void draw(){drawZone(-1);}
        void drawZone(const int&);
        int handle(int event);
        void setCutWin(CutWindow* ow);
        CutWindow* getCutWin();
        void save(xmlNodePtr parentNode);
        void load(xmlNodePtr);
        void resize(int x, int y, int w, int h);
        inline void setID(const unsigned int& id){m_id=id;}
        inline const unsigned int& getID(){return m_id;}
        inline void setAlpha(const int& alpha) { 
            m_cutProps[m_curCutProps].m_alpha=alpha;
            recomputePixels();
        }
        inline const int& getAlpha(){return m_cutProps[m_curCutProps].m_alpha;}
        inline void setTransformation(const ZONE_TRANSFO& transfo) { 
            m_cutProps[m_curCutProps].m_transfo=transfo;
            recomputePixels();
        }
        inline const ZONE_TRANSFO& getTransformation() { 
            return m_cutProps[m_curCutProps].m_transfo;
        }
        inline void setShape(const ZONE_SHAPE& shape) {
            m_cutProps[m_curCutProps].m_shape=shape;
            recomputePixels();
        }
        inline const ZONE_SHAPE& getShape() { 
            return m_cutProps[m_curCutProps].m_shape;
        }
        void setScene(const int& scene);
        void setInputEffect(const ZONE_INPUT_EFFECT& effect);
        inline const ZONE_INPUT_EFFECT& getEffect() { 
            return m_cutProps[m_curCutProps].m_effect;
        }
        void setColor(const ZONE_COLOR& color) { 
            m_cutProps[m_curCutProps].m_color=color;
        }
        inline const ZONE_COLOR& getColor() { 
            return m_cutProps[m_curCutProps].m_color;
        }
        void setVisible(const int& vis=-1);
        void setUpdate(const int& up=-1);
        inline int getUpdate(){return m_updateInScene;}
        inline ZONE_UPDATE getZoneUpdate() { 
            return ZONE_UPDATE(m_updateInScene>=0);
        }
        void setContent(const ZONE_CONTENT& cont);
        inline const ZONE_CONTENT& getContent() { 
            return m_content;
        }
        inline void learnMidi(){m_midiLearning=true;}
        void refreshCutWin();
        void startDraggingZone();
        void forceDraggingZone();
        inline void setRateReached(){m_rateReached=true;}

        void processMidi(const int& midiType, 
                         const int& midiChannel, 
                         const int& midiControl,
                         const int& midiValue);

        void recomputePixels();

    private:
        void recreateImage();

    private:
        unsigned int m_id;
//        CutWindow* m_ovWin;
//        std::string m_ovWinName;
        Fl_RGB_Image* m_image;
        uchar* m_imageData;
/*
        int m_winX, m_winY;
        int m_winW, m_winH;
*/
        std::map<int, CutProps> m_cutProps;
        static const int GLOBAL_PROPS=-1;
        int m_curCutProps;
        int m_currentScene;

        ZONE_STATE m_state;
        bool m_editing;
        bool m_highlight;
        bool m_dragging;
        bool m_forcedDragging;
        float m_dragX, m_dragY;
        float m_dragW, m_dragH;
        float m_dragStartX, m_dragStartY;
        float m_scaleX, m_scaleY;
        int m_nbPixPerRow;
        std::vector<int> m_srcIndices;
        std::vector<std::vector<int> > m_srcCoords;
        std::vector<int> m_destIndices;
/*
        int m_alpha;
        ZONE_TRANSFO m_transfo;
        ZONE_SHAPE m_shape;
*/
        bool m_rateReached;
//        ZONE_COLOR m_color;
        ZONE_CONTENT m_content;
        bool m_midiLearning;
        int m_midiControl;
        int m_midiChannel;
        int m_midiType;
//        ZONE_INPUT_EFFECT m_effect;
        timeval m_effectDelay;
        timeval m_effectStartTime;
        bool m_hiddenByEffect;
        bool m_effectDelayOver;
        bool m_initialized;
        float* m_coordImage;
        int m_visibleInScene;
        int m_updateInScene;
};


#endif

