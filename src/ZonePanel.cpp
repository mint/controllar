/***************************************************************************
 *  ZonePanel.cpp
 *  Part of 
 *  2016-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "ZonePanel.hpp"
#include <FL/Fl_Button.H>
#include <FL/fl_draw.H>
#include <algorithm>
#include <iostream>
#include "Controlar.hpp"
#include "ZoneWidget.hpp"

using namespace std;

ZonePanel::ZonePanel(): FlipGroup(0, 0, 420, 200) {
    int sep=5;
    int butH=20;
    int offY=sep;
    int brW=200;
    int brH=160;
    int labW=60;
    m_winsBr = new Fl_Hold_Browser(sep, offY, brW, brH, "window:");
    m_winsBr->align(FL_ALIGN_TOP);
    m_winsBr->has_scrollbar(Fl_Browser_::VERTICAL_ALWAYS);
    m_winsBr->callback(statWinBr, this);
    addWid(m_winsBr);

    //transformations
    Fl_Pack* radButs=new Fl_Pack(2*sep+brW, sep,(butH+sep)*4+50, 
                                 butH,"Transfo");
    add(radButs);
    radButs->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
    radButs->type(Fl_Pack::HORIZONTAL);
    radButs->spacing(sep);
    radButs->box(FL_NO_BOX);
    radButs->add(new Fl_Group(0, 0, labW, butH, ""));
    for(int i=0; i<ZoneWidget::NB_TRANSFO; ++i) {
        m_transfos[i]=ZoneWidget::ZONE_TRANSFO(i);
        m_transfoButs[i]= new Fl_Radio_Button(0, 0, butH, butH, "");
        m_transfoButs[i]->callback(statZoneTransfo, &m_transfos[i]);
        radButs->add(m_transfoButs[i]);
    }
    m_transfoButs[0]->label("@8->");
    m_transfoButs[1]->label("@6->");
    m_transfoButs[2]->label("@2->");
    m_transfoButs[3]->label("@4->");

    //shapes
    radButs=new Fl_Pack(2*sep+brW, sep*2+butH, 0, butH, "Shape");
    add(radButs);
    radButs->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
    radButs->type(Fl_Pack::HORIZONTAL);
    radButs->spacing(sep);
    radButs->box(FL_NO_BOX);
    radButs->add(new Fl_Group(0, 0, labW, butH, ""));
    for(int i=0; i<ZoneWidget::NB_SHAPES; ++i) {
        m_shapes[i]=ZoneWidget::ZONE_SHAPE(i);
        m_shapeButs[i] = new Fl_Radio_Button(0, 0, butH, butH, "");
        m_shapeButs[i]->callback(statZoneShape, &m_shapes[i]);
        radButs->add(m_shapeButs[i]);
    }
    m_shapeButs[0]->label("@-1square");
    m_shapeButs[1]->label("@-1circle");
    m_shapeButs[2]->label("@-1||");
    
    //alpha
    radButs=new Fl_Pack(2*sep+brW, sep+(sep+butH)*2, 
                         (butH+sep)*3+60, butH, "Alpha");
    add(radButs);
    radButs->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
    radButs->type(Fl_Pack::HORIZONTAL);
    radButs->spacing(sep);
    radButs->box(FL_NO_BOX);
    radButs->add(new Fl_Group(0, 0, labW, butH, ""));
    m_alphaSlider=new Fl_Slider(0, 0, (butH+sep)*3, butH, "");
    m_alphaSlider->callback(statZoneAlpha, this);
    m_alphaSlider->type(FL_HORIZONTAL);
    m_alphaSlider->bounds(0, 255);
    radButs->add(m_alphaSlider);

    //colors
    radButs=new Fl_Pack(2*sep+brW, sep+(sep+butH)*3, 
                         (butH+sep)*2+50, butH, "Color");
    add(radButs);
    radButs->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
    radButs->type(Fl_Pack::HORIZONTAL);
    radButs->spacing(sep);
    radButs->box(FL_NO_BOX);
    radButs->add(new Fl_Group(0, 0, labW, butH, ""));
    for(int i=0; i<ZoneWidget::NB_COLORS; ++i) {
        m_colors[i]=ZoneWidget::ZONE_COLOR(i);
        m_colorButs[i] = new Fl_Radio_Button(0,0, 
                                             butH, butH, "");
        m_colorButs[i]->callback(statZoneColor, &m_colors[i]);
        radButs->add(m_colorButs[i]);
    }
    m_colorButs[0]->label("N");
    m_colorButs[1]->label("I");

    //activity 
    radButs=new Fl_Pack(2*sep+brW, sep+(sep+butH)*4, 
                         (butH+sep)*3+50, butH, "Activity");
    add(radButs);
    radButs->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
    radButs->type(Fl_Pack::HORIZONTAL);
    radButs->spacing(sep);
    radButs->box(FL_NO_BOX);
    radButs->add(new Fl_Group(0, 0, 60, butH, ""));
    for(int i=0; i<ZoneWidget::NB_EFFECTS; ++i) {
        m_inputs[i]=ZoneWidget::ZONE_INPUT_EFFECT(i);
        m_inputButs[i] = new Fl_Radio_Button(0, 0, butH, butH, "");
        m_inputButs[i]->callback(statZoneInput, &m_inputs[i]);
        radButs->add(m_inputButs[i]);
    }
    m_inputButs[0]->label("-");
    m_inputButs[1]->label("S");
    m_inputButs[2]->label("H");
    Fl_Button* learnBut = new Fl_Button(0, 0, 50, butH, "Learn");
    radButs->add(learnBut);
    learnBut->callback(statZoneLearn,this);

    //content
    radButs=new Fl_Pack(2*sep+brW, sep+(sep+butH)*6, 
                         (butH+sep)*3+50, butH, "Content");
    add(radButs);
    radButs->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
    radButs->type(Fl_Pack::HORIZONTAL);
    radButs->spacing(sep);
    radButs->box(FL_NO_BOX);
    radButs->add(new Fl_Group(0, 0, 60, butH, ""));
    for(int i=0; i<ZoneWidget::NB_CONTENTS; ++i) {
        m_contents[i]=ZoneWidget::ZONE_CONTENT(i);
        m_contentButs[i] = new Fl_Radio_Button(0, 0, butH*3, butH, "");
        m_contentButs[i]->callback(statZoneContent, &m_contents[i]);
        radButs->add(m_contentButs[i]);
    }
    m_contentButs[0]->label("global");
    m_contentButs[1]->label("local");

    //update
    radButs=new Fl_Pack(2*sep+brW, sep+(sep+butH)*5, 
                         (butH+sep)*3+50, butH, "Update");
    add(radButs);
    radButs->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
    radButs->type(Fl_Pack::HORIZONTAL);
    radButs->spacing(sep);
    radButs->box(FL_NO_BOX);
    radButs->add(new Fl_Group(0, 0, 60, butH, ""));
    for(int i=0; i<ZoneWidget::NB_UPDATES; ++i) {
        m_updates[i]=ZoneWidget::ZONE_UPDATE(i);
        m_updateButs[i] = new Fl_Radio_Button(0, 0, butH*3, butH, "");
        m_updateButs[i]->callback(statZoneUpdate, &m_updates[i]);
        radButs->add(m_updateButs[i]);
    }
    m_updateButs[0]->label("all");
    m_updateButs[1]->label("current");

    //delete
    Fl_Button* delBut = new Fl_Button(sep, h()-sep-butH, 60, butH, "Delete");
    delBut->callback(statZoneDelete,this);
    add(delBut);
}

void ZonePanel::setWindowList(vector<CutWindow*>& wins) {
    m_winsBr->clear();
    m_winsBr->add("none", NULL);
    vector<CutWindow*>::iterator itWin = wins.begin();
    for(; itWin!=wins.end(); ++itWin) {
        m_winsBr->add(((*itWin)->getName()).c_str(), (*itWin));
    }
    m_winsBr->value(1);
}

void ZonePanel::setZone(ZoneWidget* z) {
    m_curZone=z;

    //retrieve values 
    if(m_curZone) {
        CutWindow* cutW = m_curZone->getCutWin();
        if(cutW) {
            string cutWN = cutW->getName();
            for(int i=1; i<=m_winsBr->size(); ++i) {
                if(cutWN.compare(m_winsBr->text(i))==0) {
                    m_winsBr->value(i);
                }
            }
        }
        else {
            m_winsBr->value(1);
        }

        for(int i=0; i<int(ZoneWidget::NB_TRANSFO); ++i) {
            m_transfoButs[i]->value(0);
        }
        m_transfoButs[int(m_curZone->getTransformation())]->value(1);
        for(int i=0; i<int(ZoneWidget::NB_SHAPES); ++i) {
            m_shapeButs[i]->value(0);
        }
        m_shapeButs[int(m_curZone->getShape())]->value(1);
        for(int i=0; i<int(ZoneWidget::NB_COLORS); ++i) {
            m_colorButs[i]->value(0);
        }
        m_colorButs[int(m_curZone->getColor())]->value(1);
        for(int i=0; i<int(ZoneWidget::NB_EFFECTS); ++i) {
            m_inputButs[i]->value(0);
        }
        m_inputButs[int(m_curZone->getEffect())]->value(1);
        for(int i=0; i<int(ZoneWidget::NB_UPDATES); ++i) {
            m_updateButs[i]->value(0);
        }
        m_updateButs[int(m_curZone->getZoneUpdate())]->value(1);
        for(int i=0; i<int(ZoneWidget::NB_CONTENTS); ++i) {
            m_contentButs[i]->value(0);
        }
        m_contentButs[int(m_curZone->getContent())]->value(1);
        m_alphaSlider->value(m_curZone->getAlpha());
    }
}

void ZonePanel::cbWinBr(Fl_Widget*) {
    m_curZone->setCutWin((CutWindow*)(m_winsBr->data(m_winsBr->value())));
    refresh();
}

void ZonePanel::cbZoneTransfo(const ZoneWidget::ZONE_TRANSFO& trans) {
    m_curZone->setTransformation(trans);
    refresh();
}

void ZonePanel::cbZoneShape(const ZoneWidget::ZONE_SHAPE& shape) {
    m_curZone->setShape(shape);
    refresh();
}

void ZonePanel::cbZoneAlpha() {
    m_curZone->setAlpha(m_alphaSlider->value());
    refresh();
}

void ZonePanel::cbZoneColor(const ZoneWidget::ZONE_COLOR& color) {
    m_curZone->setColor(color);
    refresh();
}

void ZonePanel::cbZoneInput(const ZoneWidget::ZONE_INPUT_EFFECT& inp) {
    m_curZone->setInputEffect(inp);
    refresh();
}

void ZonePanel::cbZoneUpdate(const ZoneWidget::ZONE_UPDATE& up) {
    Controlar::getInstance()->cbZoneUpdate(up);
    refresh();
}

void ZonePanel::cbZoneContent(const ZoneWidget::ZONE_CONTENT& cont) {
    m_curZone->setContent(cont);
    refresh();
}

ZonePanel* ZonePanel::getInstance() {
    static ZonePanel instance;
    return &instance;
}

