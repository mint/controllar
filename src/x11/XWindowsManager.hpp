/***************************************************************************
 *  XWindowsManager.hpp
 *  Part of Over 
 *  2013  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef XWindowsManager_h
#define XWindowsManager_h

#include <vector>
#include <string>
#include <X11/Xlib.h>
#include <X11/Xatom.h>

#include "../CutWindowsManager.hpp"
#include "XWindow.hpp"

class XWindowsManager : public CutWindowsManager {
    public :
        XWindowsManager();
        virtual ~XWindowsManager();
        virtual void updateWindowsList();

        Window* getWinList(Display* disp, unsigned long* len);
        char* getWinName(Display* disp, Window);

    protected:
        Display* m_disp;
};

#endif

