/***************************************************************************
 *  XWindow.cpp
 *  Part of 
 *  2016-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "XWindow.hpp"
#include <iostream>

using namespace std;

XWindow::XWindow(CutWindowsManager* man, 
                 Display* disp, 
                 const Window& win): CutWindow(man), 
                                     m_disp(disp), 
                                     m_win(win) { 
    Window root;
    unsigned int bw, d, w, h;
    XGetGeometry(m_disp, m_win, &root, &m_posX, &m_posY, 
                 &w, &h, &bw, &d);
    m_width=w;
    m_height=h;
    m_offsetX=0;
    m_offsetY=0;
    m_pixPerRow=m_width;
    m_isBGR=false;
    m_needsOffset=true;
    m_grabbed=false;
    m_defaultImgData = new uchar[m_width*m_height*3];
}

int XWindow::computeNbPixPerRow(const int& srcW, const int& srcH) {
    XWindowAttributes attrs;
    XGetWindowAttributes(m_disp, m_win, &attrs);
    m_width=attrs.width;
    m_height=attrs.height;
    m_pixPerRow=m_width;
    return m_width;
}

void XWindow::getPixels(const int& srcX, const int& srcY, 
                        const int& srcW, const int& srcH, 
                        const vector<int>& srcIndices,
                        const vector<vector<int> >& srcCoords,
                        const vector<int>& destIndices,
                        const uchar& alpha,
                        const ZoneWidget::ZONE_COLOR& color,
                        uchar* destImg) {
    XImage* ximg = XGetImage(m_disp, m_win, 
                             srcX, srcY, 
                             srcW, srcH, 
                             AllPlanes, XYPixmap);
    if(ximg!=NULL) {
        unsigned long red_mask = ximg->red_mask;
        unsigned long green_mask = ximg->green_mask;
        unsigned long blue_mask = ximg->blue_mask;

        vector<int>::const_iterator itId = srcIndices.begin();
        vector<vector<int> >::const_iterator itCo = srcCoords.begin();
        vector<int>::const_iterator itDe = destIndices.begin();
        for(; itCo!=srcCoords.end(); ++itCo) {
            if((*itCo)[0]>=0) {//get and copy pixel
                unsigned long pixel = XGetPixel(ximg, (*itCo)[0], (*itCo)[1]);
                destImg[(*itDe)] = (color==ZoneWidget::NORMAL) 
                                        ?(pixel & red_mask) >> 16
                                        :255-((pixel & red_mask) >> 16);
                ++itDe;
                destImg[(*itDe)] = (color==ZoneWidget::NORMAL) 
                                        ?(pixel & green_mask) >> 8
                                        :255-((pixel & green_mask) >> 8);
                ++itDe;
                destImg[(*itDe)] = (color==ZoneWidget::NORMAL) 
                                        ?(pixel & blue_mask)
                                        :255-(pixel & blue_mask);
                ++itDe;
                destImg[(*itDe)] = alpha;
                ++itDe;
            }
            else { //black
                destImg[(*itDe)] = 0;
                ++itDe;
                destImg[(*itDe)] = 0;
                ++itDe;
                destImg[(*itDe)] = 0;
                ++itDe;
                destImg[(*itDe)] = 0;
                ++itDe;
            }
        }
        XDestroyImage(ximg);
    }
}

void XWindow::releaseImage() {
    if(m_grabbed) {
        if(m_ximg) {
            XDestroyImage(m_ximg);
        }
        m_grabbed=false;
    }
}

uchar* XWindow::grabImage() {
    if(!m_grabbed) {
        m_ximg = XGetImage(m_disp, m_win, 
                           0, 0, 
                           m_width, m_height, 
                           AllPlanes, ZPixmap);
        if(m_ximg) {
            m_imgData=(uchar*)m_ximg->data;
        }
        else {
            m_imgData=m_defaultImgData;
        }
        m_grabbed=true;
    }
    return m_imgData;
}

void XWindow::storeImage(const int& id) {
    int size = m_pixPerRow*m_height*4;
    if(m_storedImgData.find(id)!=m_storedImgData.end()) {
        delete [] m_storedImgData[id];
    }
    m_storedImgData[id] = new uchar[size];
    memcpy(m_storedImgData[id], m_imgData, size);
}

uchar* XWindow::retrieveImage(const int& id) {
    map<int, uchar*>::iterator itSt = m_storedImgData.find(id);
    if(itSt!=m_storedImgData.end()) {
        return itSt->second;
    }
    return m_imgData;
}


    
