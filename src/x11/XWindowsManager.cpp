/***************************************************************************
 *  XWindowsManager.cpp
 *  Part of 
 *  2016-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "XWindowsManager.hpp"
#include <iostream>
#include <stdlib.h>

using namespace std;

XWindowsManager::XWindowsManager(): CutWindowsManager() {
    m_disp = XOpenDisplay(NULL);
    if (!m_disp) {
        cout<<"No display found"<<endl;
    }
}

XWindowsManager::~XWindowsManager() {
    XCloseDisplay(m_disp);
}

void XWindowsManager::updateWindowsList() {
    map<string, CutWindow*> prevWins;
    vector<CutWindow*>::iterator itWin=m_windowList.begin();
    for(; itWin!=m_windowList.end(); ++itWin) {
        prevWins[(*itWin)->getName()]=(*itWin);
    }
    m_windowList.clear();
    unsigned long len;
    Window *list = getWinList(m_disp, &len);
    for(int i=0;i<(int)len;i++) {
        char* name = getWinName(m_disp, list[i]);
        CutWindow* newWin=NULL;
        if(prevWins.find(string(name))!=prevWins.end()) {
            newWin = prevWins[string(name)];
        }
        else {
            newWin = new XWindow(this, m_disp, list[i]);
        }
        newWin->setName(string(name));
        m_windowList.push_back(newWin);
        free(name);
    }
    XFree(list);
}

char* XWindowsManager::getWinName(Display* disp, Window win) {
    Atom prop = XInternAtom(disp,"_NET_WM_NAME",false);
	Atom utf8Atom = XInternAtom(disp,"UTF8_STRING",false);
	Atom type;
    int form;
    unsigned long remain, len;
    unsigned char *list;

    if (XGetWindowProperty(disp,win,prop,0,65536,false,utf8Atom,
            &type,&form,&len,&remain,&list) != Success) {
        cout<<"Error getting window name"<<endl;
        return NULL;
    }
    return (char*)list;
}

Window* XWindowsManager::getWinList(Display* disp, unsigned long* len) {
    Atom prop = XInternAtom(disp,"_NET_CLIENT_LIST",False), type;
    int form;
    unsigned long remain;
    unsigned char *list;
    if(XGetWindowProperty(disp,XDefaultRootWindow(disp), 
                          prop,0,1024,False,XA_WINDOW,
                          &type,&form,len,&remain,&list) != Success) {
        cout<<"Error getting windows list"<<endl;
        return 0;
    }
    return (Window*)list;
}


