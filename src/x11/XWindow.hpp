/***************************************************************************
 *  CutWindow.hpp
 *  Part of Over 
 *  2013  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef XWindow_h
#define XWindow_h

#include <vector>
#include <string>

#include "../CutWindow.hpp"

#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/Xutil.h>

class XWindow: public CutWindow {
    public :
        XWindow(CutWindowsManager* man, Display* disp, const Window& win);
        virtual ~XWindow(){}
        virtual void getPixels(const int& x, const int& y, 
                               const int& sx, const int& sy,
                               const std::vector<int>& srcIndices,
                               const std::vector<std::vector<int> >& srcCoords,
                               const std::vector<int>& destIndices,
                               const uchar& alpha,
                               const ZoneWidget::ZONE_COLOR& color,
                               uchar* destImg);
        int computeNbPixPerRow(const int& srcW, const int& srcH);
        virtual uchar* grabImage();
        virtual void releaseImage();

        virtual void storeImage(const int& id);
        virtual uchar* retrieveImage(const int& id);

    protected:
        Display* m_disp;
        Window m_win;
        XImage* m_ximg;
};

#endif

