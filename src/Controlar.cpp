/***************************************************************************
 *  Controlar.cpp
 *  Part of ControllAR
 *  2016-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "Controlar.hpp"

#include <iostream>
#include <sstream>
#include <stdio.h>
#include <inttypes.h>
#include <unistd.h>
#include <portmidi.h>
#include <porttime.h>
#include <FL/fl_ask.H>
#include <FL/Fl.H>
#include "osc/osc/OscReceivedElements.h"
#include "osc/osc/OscOutboundPacketStream.h"
#include "MainPanel.hpp"
#include "ZonePanel.hpp"

#ifdef OSX
    #include "mac/MacWindowsManager.hpp"
#else 
    #ifdef WINDOWS
        #include "win/WinWindowsManager.hpp"
    #else 
        #include "x11/XWindowsManager.hpp"
    #endif
#endif

using namespace std;

void idleFunc(void* data) {
    Controlar::getInstance()->update();
    usleep(10000);    
}
 
void* oscThreadFunction(void* pvParam) {
    Controlar::getInstance()->m_socket->Run();
    return 0;
}

#ifdef GL
Controlar::Controlar(): Fl_Gl_Window(800, 600, "ControllAR") {
#else 
Controlar::Controlar(): Fl_Double_Window(800, 600, "ControllAR") {
#endif
    Fl::set_color(FL_BACKGROUND_COLOR, 200, 200, 200);
    Fl::set_color(FL_BACKGROUND2_COLOR, 150, 150, 150);
    Fl::set_color(FL_SELECTION_COLOR, 250, 250, 250);
    box(FL_FLAT_BOX);
    end();

    m_drawing=false;
    m_flipped=false;
    m_fullscreen=false;
    m_duplicating=false;
    m_midiLearning=false;
    m_askedScene=-1;
    m_displayScale=1;

    m_alphaLevels[0] = 65;
    m_alphaLevels[1] = 125;
    m_alphaLevels[2] = 190;
    m_alphaLevels[3] = 255;
    for(int i=0;i<10;++i) {
        m_menuValues[i]=i;
    }

    //add a first set
    //m_currentSet=0;
    //m_zonesSets.push_back(map<unsigned int, ZoneWidget*>());
    m_scenes[0]="scene";
    m_currentScene=0;

    //create menus
    m_zoneMenu = new FlipMenu();
    m_zoneMenu->hide();
    m_zoneMenu->end();
    add(m_zoneMenu);
    m_mainMenu = new FlipMenu();
    m_mainMenu->end();
    m_mainMenu->hide();
    add(m_mainMenu);
    m_mainMenu->addWithSub("File/Open", statOpen, this);
    m_mainMenu->addWithSub("File/Save", statSave, this);
    m_mainMenu->addWithSub("File/Save As", statSaveAs, this);
    m_mainMenu->addWithSub("View/FullScreen", statFullscreen, this);
    m_mainMenu->addWithSub("View/Mirror", statFlip, this);
    m_mainMenu->addWithSub("Scenes/Next", statSceneNext, this);
    m_mainMenu->addWithSub("Scenes/Prev", statScenePrev, this);
    m_mainMenu->addWithSub("Scenes/Delete", statSceneDel, this);
    m_mainMenu->addWithSub("Scenes/Learn Input", statSceneLearn, this);
    m_mainMenu->addSub("Midi Input");
    m_mainMenu->add("Quit", statQuit, this);

    m_mainPanel = MainPanel::getInstance();
    //add(m_mainPanel);
    m_mainPanel->hide();
    m_zonePanel= ZonePanel::getInstance();
    //add(m_zonePanel);
    m_zonePanel->hide();

    //initialize main loop function
    Fl::add_idle(idleFunc, NULL);

    //create windows manager and get display scale if needed
    #ifdef OSX
        m_winsMan = new MacWindowsManager();
    #else
        #ifdef WINDOWS
            m_winsMan = new WinWindowsManager();
        #else
            m_winsMan = new XWindowsManager();
        #endif
    #endif
    
    updateTitle();

    //initialise midi
    m_midiStream=NULL;
    Pm_Initialize();
    detectMidiDevices();
    Pt_Start(1, NULL, NULL);

    //initialize osc 
    try { 
        m_socket = new UdpListeningReceiveSocket(
                            IpEndpointName(IpEndpointName::ANY_ADDRESS, 8330),
                            this);
        pthread_mutex_init(&m_zonesMutex, NULL);
        pthread_create(&m_oscThread, NULL, oscThreadFunction , this);   
    }
    catch(const exception& e) {
        DEBUG("Error in the OscManager "<<e.what());
    }
}

void Controlar::refreshWindowList() {
    //recreate windows list
    m_winsMan->updateWindowsList();

    //clean all zones and reassign windows
    map<unsigned int, ZoneWidget*>::iterator itZo = m_zones.begin();
    for(; itZo!=m_zones.end(); ++itZo) {
        itZo->second->refreshCutWin();
    }

    //rebuild zone menu
    m_zoneMenu->clear();
    vector<CutWindow*>::iterator itWin = m_winsMan->editWindowList().begin();
    for(; itWin!=m_winsMan->editWindowList().end(); ++itWin) {
        m_zoneMenu->addWithSub((string("Select/")+(*itWin)->getName()).c_str(), 
                                statZoneSelect, (*itWin));
    }
    m_zonePanel->setWindowList(m_winsMan->editWindowList());


    m_zoneMenu->addWithSub("Alpha/Opaque", statZoneAlpha, &m_alphaLevels[3]);
    m_zoneMenu->addWithSub("Alpha/Mostly Opaque", 
                           statZoneAlpha, &m_alphaLevels[2]);
    m_zoneMenu->addWithSub("Alpha/Half Transparent",  
                           statZoneAlpha, &m_alphaLevels[1]);
    m_zoneMenu->addWithSub("Alpha/Mostly Transparent",
                           statZoneAlpha, &m_alphaLevels[0]);
    m_zoneMenu->addWithSub("Transform/No Rotation",
                       statZoneTrans, &m_menuValues[ZoneWidget::NONE]);
    m_zoneMenu->addWithSub("Transform/Rotate 90",
                       statZoneTrans, &m_menuValues[ZoneWidget::ROTATE_90]);
    m_zoneMenu->addWithSub("Transform/Rotate 180",
                       statZoneTrans, &m_menuValues[ZoneWidget::ROTATE_180]);
    m_zoneMenu->addWithSub("Transform/Rotate 270",
                       statZoneTrans, &m_menuValues[ZoneWidget::ROTATE_270]);
    m_zoneMenu->addWithSub("Shape/Box",
                           statZoneShape, &m_menuValues[ZoneWidget::BOX]);
    m_zoneMenu->addWithSub("Shape/Circle",
                           statZoneShape, &m_menuValues[ZoneWidget::CIRCLE]);
    m_zoneMenu->addWithSub("Shape/Frame",
                           statZoneShape, &m_menuValues[ZoneWidget::FRAME]);
    m_zoneMenu->addWithSub("Color/Normal",
                           statZoneCol, &m_menuValues[ZoneWidget::NORMAL]);
    m_zoneMenu->addWithSub("Color/Inverse",
                           statZoneCol, &m_menuValues[ZoneWidget::INVERT]);
    m_zoneMenu->addWithSub("Visible/All Scenes",
                           statZoneVisible, 
                           &m_menuValues[ZoneWidget::VISIBLE_ALL]);
    m_zoneMenu->addWithSub("Visible/Current Scene Only",
                           statZoneVisible, 
                           &m_menuValues[ZoneWidget::VISIBLE_CURRENT]);
    m_zoneMenu->addWithSub("Update/All Scenes",
                           statZoneUpdate, 
                           &m_menuValues[ZoneWidget::UPDATE_ALL]);
    m_zoneMenu->addWithSub("Update/Current Scene Only",
                           statZoneUpdate, 
                           &m_menuValues[ZoneWidget::UPDATE_CURRENT]);
    m_zoneMenu->addWithSub("Content/Global",
                           statZoneContent, 
                           &m_menuValues[ZoneWidget::CONTENT_GLOBAL]);
    m_zoneMenu->addWithSub("Content/Local",
                           statZoneContent, 
                           &m_menuValues[ZoneWidget::CONTENT_LOCAL]);
    m_zoneMenu->addWithSub("MIDI Input/Learn",
                           statZoneMidiLearn, this);
    m_zoneMenu->addWithSub("MIDI Input/No effect",
                   statZoneMidiEffect, &m_menuValues[ZoneWidget::NO_EFFECT]);
    m_zoneMenu->addWithSub("MIDI Input/Show",
                   statZoneMidiEffect, &m_menuValues[ZoneWidget::SHOW]);
    m_zoneMenu->addWithSub("MIDI Input/Hide",
                   statZoneMidiEffect, &m_menuValues[ZoneWidget::HIDE]);

    m_zoneMenu->add("Clear", statZoneClear, this);
    m_zoneMenu->add("Delete", statZoneDelete, this);

    m_mainMenu->hide();
    m_zoneMenu->hide();
}

void Controlar::update() {
    Controlar::getInstance()->damage(FL_DAMAGE_USER1);
    parseMidi();
}


void Controlar::parseMidi() {
    PmEvent buffer[10];
    vector<PortMidiStream*>::iterator itSt = m_midiStreamsVec.begin();
    for(; itSt!=m_midiStreamsVec.end();++itSt) {
        int nb = Pm_Read((*itSt), buffer, 10);
        for(int i=0;i<nb;++i) {
            long status = Pm_MessageStatus(buffer[i].message);
            int msgType = status >> 4;
            int msgChannel = (status & 15) + 1;
            int control = Pm_MessageData1(buffer[i].message);
            int value = Pm_MessageData2(buffer[i].message);
            if(m_midiLearning) {
                m_midiType = msgType;
                m_midiChannel = msgChannel;
                m_midiControl = control;
                m_midiLearning=false;
            }
            if(msgType==m_midiType 
                    && msgChannel==m_midiChannel 
                    && control==m_midiControl) {
                setScene(value);
            } 
            map<unsigned int, ZoneWidget*>::iterator itZo = m_zones.begin();
            for(; itZo!=m_zones.end(); ++itZo) {
                itZo->second->processMidi(msgType, msgChannel, 
                                          control, value);
            }
        }
    }
}

void Controlar::openMidiDevice(const std::string& devName) {
    map<string, int>::iterator itDev=m_midiDevMap.find(devName);
    if(itDev!=m_midiDevMap.end()) {
        if(m_midiStreamsMap.find(devName)!=m_midiStreamsMap.end()) {
            Pm_Close(m_midiStreamsMap[devName]);
        }
        m_midiStreamsMap[devName]=NULL;
        Pm_OpenInput(&m_midiStreamsMap[devName], 
                     itDev->second, NULL, 100, NULL, NULL);
    }
    refreshMidiStreams();
}

void Controlar::closeMidiDevice(const std::string& devName) {
    map<string, PortMidiStream*>::iterator itSt=m_midiStreamsMap.find(devName);
    if(itSt!=m_midiStreamsMap.end()) {
        Pm_Close(itSt->second);
        m_midiStreamsMap.erase(devName);
    }
    refreshMidiStreams();
}

void Controlar::refreshMidiStreams() {
    m_midiStreamsVec.clear();
    map<string, PortMidiStream*>::iterator itSt=m_midiStreamsMap.begin();
    for(; itSt!=m_midiStreamsMap.end(); ++itSt) {
        m_midiStreamsVec.push_back(itSt->second);
    }
}

void Controlar::detectMidiDevices() {

    Pm_Terminate();
    Pm_Initialize();
    int nbDevices = Pm_CountDevices();
/*
    FlipMenu* midiMenu = m_mainMenu->getSub("Midi Input");
    midiMenu->clear();
    m_midiDevices.clear();
    m_midiDeviceNames.clear();
    m_midiStreamsMap.clear();
*/
    vector<bool> devOps;
    vector<string> devNames;
    for(int i=0; i<nbDevices; ++i) {
        const PmDeviceInfo* info = Pm_GetDeviceInfo(i);
        if(info->input>0) {
            string devName = string(info->name);
/*
            m_midiDevices.push_back(i);
            m_midiDeviceNames.push_back(devName);
            midiMenu->add(devName, statMidiDev, &m_midiDevices.back());
*/
            m_midiDevMap[devName]=i;
            devNames.push_back(devName);
            if(m_midiStreamsMap.find(devName)!=m_midiStreamsMap.end()) {
                openMidiDevice(devName);
                devOps.push_back(true);
            }
            else {
                devOps.push_back(false);
            }
        }
    }
    m_mainPanel->setMidiDevices(devNames, devOps);
/*
    if(m_currentMidiDeviceName.compare("")!=0) {
        setMidiDeviceFromName(m_currentMidiDeviceName);
    }
*/
}

ZoneWidget* Controlar::startDuplicating(ZoneWidget* dupWid) {
    m_duplicatedZone = new ZoneWidget();
    m_duplicatedZone->copy(dupWid);
    addZone(m_duplicatedZone);
    m_duplicatedZone->forceDraggingZone();
    return m_duplicatedZone;
}

int Controlar::handle(int event) {
    int res=0;
    if(!m_drawing) {
        res=Fl_Group::handle(event);
    }
    if(res>0) {
        redraw();
    }
    else {
        switch(event) {
            case FL_KEYDOWN: {
                if(Fl::event_command()) {
                    switch(Fl::event_key()) {
                        case 'o': cbOpen(); break;
                        case 's': cbSave(); break;
                        case 'f': cbFullscreen(); break;
                        case 'm': cbFlip(); break;
                        case 'q': cbQuit(); break;
                        default:break;
                    }
                    res=1;
                }
            }break; 
            case FL_PUSH: 
                if(m_duplicating) {
                    m_duplicating=false;
                }
                switch(Fl::event_button()) {
                    case FL_LEFT_MOUSE: {
                        m_drawStartX=Fl::event_x();
                        m_drawStartY=Fl::event_y();
                        m_drawCurX=Fl::event_x();
                        m_drawCurY=Fl::event_y();
                        m_drawing=true;
                        m_drawingGroup=false;
                        if(Fl::event_shift()) {
                            m_drawingGroup=true;
                        }
                        m_mainMenu->hide();
                        m_zoneMenu->hide();
                        m_mainPanel->hide();
                        m_zonePanel->hide();
                        redraw();
                    }break;
                    case FL_RIGHT_MOUSE: {
                        detectMidiDevices();
                        insert(*m_mainMenu, children());
                        int menPosX = (Fl::event_x()>w()-m_mainPanel->w())? 
                                Fl::event_x()-m_mainPanel->w():Fl::event_x();
                        int menPosY = (Fl::event_y()>h()-m_mainPanel->h())? 
                                Fl::event_y()-m_mainPanel->h():Fl::event_y();
                        //m_mainMenu->position(menPosX, menPosY);
                        //m_mainMenu->show();
                        //m_zoneMenu->hide();
                        m_mainPanel->position(menPosX, menPosY);
                        insert(*m_mainPanel,children());
                        m_mainPanel->show();
                    }break;
                    default:break;
                }
                res=1;
            break;
            case FL_DRAG: 
                m_drawCurX=Fl::event_x();
                m_drawCurY=Fl::event_y();
            break;
            case FL_RELEASE: 
                switch(Fl::event_button()) {
                    case FL_LEFT_MOUSE: {
                        int minPixels=20;
                        int drawStopX = Fl::event_x();
                        int drawStopY = Fl::event_y();
                        if(m_drawing) {
                            if(abs(m_drawStartX-drawStopX)>minPixels && 
                                    abs(m_drawStartY-drawStopY)>minPixels) {
                                if(m_drawingGroup) {
                                    addGroup(new GroupWidget(m_drawStartX, 
                                                           m_drawStartY, 
                                                  abs(m_drawStartX-drawStopX), 
                                                  abs(m_drawStartY-drawStopY)));
                                }
                                else {
                                    addZone(new ZoneWidget(
                                              m_drawStartX-ZoneWidget::OUTER, 
                                              m_drawStartY-ZoneWidget::OUTER, 
                                              abs(m_drawStartX-drawStopX)
                                                +ZoneWidget::OUTER*2, 
                                              abs(m_drawStartY-drawStopY)
                                                +ZoneWidget::OUTER*2)); 
                                }
                            }
                        }
                        m_drawing=false;
                        res=1;
                    }break;
                    default:break;
                }
            break;
            default:break;
        }
    }
    redraw();
    return res;
}

void Controlar::ProcessMessage(const osc::ReceivedMessage& mess,
                                const IpEndpointName& remoteEndpoint) {
    try {
        string name = string(mess.AddressPattern());
        if(name.find("/controllar/")!=string::npos) {
            if(name.find("/scene")!=string::npos && mess.ArgumentCount()>0) {
                pthread_mutex_lock(&m_zonesMutex);
                    m_askedScene=mess.ArgumentsBegin()->AsInt32();
                pthread_mutex_unlock(&m_zonesMutex);
            }
        }
    } 
    catch(const exception& e) {
        cout<<"Exception when receiving osc message "<<e.what()<<endl;
    }
}


void Controlar::cbZone(Fl_Widget* wid) {
    refreshWindowList();

    m_clickedZone = static_cast<ZoneWidget*>(wid);
    int menPosX = (Fl::event_x()>w()-m_zonePanel->w())? 
                    Fl::event_x()-m_zonePanel->w():Fl::event_x();
    int menPosY = (Fl::event_y()>h()-m_zonePanel->h())? 
                    Fl::event_y()-m_zonePanel->h():Fl::event_y();
    //m_zoneMenu->position(menPosX, menPosY);
    //insert(*m_zoneMenu, children());
    //m_zoneMenu->show();
    m_zonePanel->setZone(m_clickedZone);
    m_zonePanel->position(menPosX, menPosY);
    m_zonePanel->refresh();
    insert(*m_zonePanel,children());
    m_zonePanel->show();
}

void Controlar::cbSceneNext() {
    if(m_scenes.find(m_currentScene+1)==m_scenes.end()) {
        m_scenes[m_currentScene+1]="scene";
    }
    setScene(m_currentScene+1);
}

void Controlar::cbScenePrev() {
    setScene(m_currentScene-1);
}

/*
void Controlar::cbSceneDuplicate() {
    //save ref to current set
    int cur=m_currentSet;

    //create new set and make it current
    m_zonesSets.push_back(map<unsigned int, ZoneWidget*>());
    setSet(m_zonesSets.size()-1);

    //copy zones 
    map<unsigned int, ZoneWidget*>::iterator itZo = m_zonesSets[cur].begin();
    for(; itZo!=m_zonesSets[cur].end(); ++itZo) {
        ZoneWidget* newZo = new ZoneWidget();
        newZo->copy(itZo->second);
        addZone(newZo);
    }
    setSet(m_currentSet);
}
*/

void Controlar::cbSceneDelete() {
/*
    if(m_zonesSets.size()>1) {
        if(fl_choice("Are you sure you want to delete the current set ?", 
                "No", "Yes", "")) {
            map<unsigned int, ZoneWidget*>::iterator itZo 
                = m_zonesSets[m_currentSet].begin();
            for(; itZo!=m_zonesSets[m_currentSet].end(); ++itZo) {
                Fl::delete_widget(itZo->second);
            }
            m_zonesSets.erase(m_zonesSets.begin()+m_currentSet);
            setSet(0);
        }
    }
*/
}

void Controlar::setScene(const int& scene) {
    if(m_scenes.find(scene)!=m_scenes.end()) {
        //store images for zones that only update on current scene
        map<unsigned int, ZoneWidget*>::iterator itZo = m_zones.begin();
        for(; itZo!=m_zones.end(); ++itZo) {
            itZo->second->setScene(scene);
        }
        //change scene
        m_currentScene=scene;
        m_mainPanel->setCurrentScene(scene);
        m_mainPanel->refresh();
        updateTitle();
    }
    m_mainMenu->hide();
}

void Controlar::updateTitle() {
    ostringstream oss;
    oss<<m_currentScene;
    m_title = "ControllAR - "+m_fileName+" - Scene "+oss.str();
    label(m_title.c_str());
}

void Controlar::cbSceneLearn() {
    m_midiLearning=true;
    m_mainMenu->hide();
}

void Controlar::cbFlip() {
    m_flipped=!m_flipped;
    resize(x(),y(),w(),h());
    m_mainMenu->hide();
    map<unsigned int, ZoneWidget*>::iterator itZo=m_zones.begin();
    for(; itZo!=m_zones.end(); ++itZo) {
        itZo->second->recomputePixels();
    }
}

void Controlar::cbFullscreen() {
    m_fullscreen=!m_fullscreen;
    if(m_fullscreen) {
        fullscreen();
    }
    else {
        fullscreen_off();
    }
    m_mainMenu->hide();
    fl_cursor(FL_CURSOR_NONE);
}

#ifdef GL
void Controlar::initGL() {
    #ifdef OSX
        glewExperimental = GL_TRUE; 
    #endif
    glewInit();
    glGenVertexArrays(1, &m_vertexArrayID);
    glBindVertexArray(m_vertexArrayID); 
    glGenBuffers(1, &m_vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
    m_vertexBufferData[0]=0;m_vertexBufferData[1]=0;m_vertexBufferData[2]=0;
    m_vertexBufferData[3]=1;m_vertexBufferData[4]=0;m_vertexBufferData[5]=0;
    m_vertexBufferData[6]=1;m_vertexBufferData[7]=1;m_vertexBufferData[8]=0;
    m_vertexBufferData[9]=0;m_vertexBufferData[10]=0;m_vertexBufferData[11]=0;
    m_vertexBufferData[12]=1;m_vertexBufferData[13]=1;m_vertexBufferData[14]=0;
    m_vertexBufferData[15]=0;m_vertexBufferData[16]=1;m_vertexBufferData[17]=0;
    glBufferData(GL_ARRAY_BUFFER, sizeof(m_vertexBufferData), 
                 m_vertexBufferData, GL_STATIC_DRAW);

    GLint compile_ok = GL_FALSE, link_ok = GL_FALSE;

    GLuint vertexShader, fragmentShader;

    //ZONE PROGRAM
    {
    vertexShader = glCreateShader(GL_VERTEX_SHADER);
    const char *vs_source =
        "#version 110\n"
        "attribute vec3 coord;\n"
        "uniform vec2 winSize;\n"
        "uniform vec2 zonePos;\n"
        "uniform vec2 zoneSize;\n"
        "uniform vec2 cutPos;\n"
        "uniform vec2 cutSize;\n"
        "uniform float mirrored;\n"
        "uniform float outer;\n"
        "varying vec2 pixRatio;\n"
        "varying vec4 pixToSides;\n" //T,B,L,R
        "void main(void) {\n"
        "   vec2 zonePixRatio;\n"
        "   if(mirrored>0.0) {\n"
        "       zonePixRatio = vec2(coord.x, coord.y);\n"
        "       pixRatio = vec2(\n"
        "               (coord.x*zoneSize.x-outer)/cutSize.x,\n"
        "               (coord.y*zoneSize.y-outer)/cutSize.y);\n"
        "   }\n"
        "   else {\n"
        "       zonePixRatio = vec2(coord.x, 1.0-coord.y);\n"
        "       pixRatio = vec2(\n"
        "               (coord.x*zoneSize.x-outer)/cutSize.x,\n"
        "               ((1.0-coord.y)*zoneSize.y-outer)/cutSize.y);\n"
        "   }\n"
        "   vec2 cutPixPos = pixRatio*cutSize;\n"
        "   vec2 pos = ((cutPixPos+cutPos)/winSize)*2.0-1.0;\n"
        "   if(mirrored>0.0) {\n"
        "       gl_Position = vec4(pos.x, pos.y, 0.0, 1.0);\n"
        "   }\n"
        "   else {\n"
        "       gl_Position = vec4(pos.x, -pos.y, 0.0, 1.0);\n"
        "   }\n"
        "   vec2 zonePixPos = zonePixRatio*zoneSize;\n"
        "   pixToSides = vec4(zonePixPos.y,\n"
        "                     zoneSize.y-zonePixPos.y,\n"
        "                     zonePixPos.x,\n"
        "                     zoneSize.x-zonePixPos.x);\n"
        "}\n";
    glShaderSource(vertexShader, 1, &vs_source, NULL);
    glCompileShader(vertexShader);
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &compile_ok);
    if (!compile_ok) {
        cout << "Error in vertex shader" << endl;
        GLint maxLength = 0;
        glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &maxLength);
        std::vector<GLchar> errorLog(maxLength);
        glGetShaderInfoLog(vertexShader, maxLength, &maxLength, &errorLog[0]);
        vector<GLchar>::iterator itC=errorLog.begin();
        for(; itC!=errorLog.end(); ++itC) {
            cout<<*itC;
        }
        cout<<endl;
    } 

    fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    const char *fs_source =
        "#version 110\n"
        "varying vec2 pixRatio;\n"
        "varying vec4 pixToSides;\n" //T,B,L,R
        "uniform sampler2D windowTexture;\n"
        "uniform sampler2D coordsTexture;\n"
        "uniform vec2 winSize;\n"
        "uniform vec2 zonePos;\n"
        "uniform vec2 zoneSize;\n"
        "uniform vec2 cutPos;\n"
        "uniform vec2 cutSize;\n"
        "uniform float mirrored;\n"
        "uniform int state;\n"//NORMAL,MOVE_CUT,RESIZE_CUT,MOVE_ZONE,RESIZE_ZONE
        "uniform float alpha;\n"
        "uniform float filled;\n"
        "uniform float invert;\n"
        "uniform float outer;\n"
        "uniform float inner;\n"
        "void main(void) {       \n"
        "   float thick=1.0;\n"
        "   if(filled>0.0) {\n"
        "       if(texture2D(coordsTexture, pixRatio).r>=0.0 \n"
        "               && pixRatio.x>0.0 && pixRatio.x<1.0 \n"
        "               && pixRatio.y>0.0 && pixRatio.y<1.0) {\n"
        "           vec4 winCol = texture2D(windowTexture, \n"
        "                         texture2D(coordsTexture, pixRatio).rg);\n"
#ifdef WINDOWS
        "           winCol = texture2D(windowTexture, pixRatio);\n"
#else
        "           winCol = vec4(winCol.b,winCol.g,winCol.r,winCol.a);\n"
#endif
        "           if(invert>0.0) {\n" 
        "               gl_FragColor=vec4(1.0-winCol.r, 1.0-winCol.g, \n"
        "                                 1.0-winCol.b, alpha);\n"
        "           }\n"
        "           else {\n" 
        "               gl_FragColor=vec4(winCol.rgb, alpha);\n"
        "           }\n"
        "       }\n"
        "       else {\n"
        "           gl_FragColor=vec4(0.0, 0.0, 0.0, 0.0);\n"
        "       }\n"
        "       if(state>0) {\n"
        //outer border
        "           if(pixToSides.x<thick ||  pixToSides.y<thick \n"
        "                   || pixToSides.z<thick || pixToSides.w<thick) { \n"
        "               if(state==3) {\n"
        "                   gl_FragColor=vec4(1.0,0.0,0.0,1.0);\n"
        "               }\n"
        "               else {\n"
        "                   gl_FragColor=vec4(1.0,1.0,1.0,1.0);\n"
        "               }\n"
        "           }\n"
        //inner     border
        "           if(((abs(pixToSides.x-inner)<thick \n"
        "                       || abs(pixToSides.y-inner)<thick)\n"
        "                   && (pixToSides.z>inner && pixToSides.w>inner))\n"
        "                  || (abs(pixToSides.z-inner)<thick \n"
        "                       || abs(pixToSides.w-inner)<thick)\n"
        "                   && (pixToSides.x>inner && pixToSides.y>inner)) {\n"
        "               if(state==1) {\n"
        "                   gl_FragColor=vec4(1.0,0.0,0.0,1.0);\n"
        "               }\n"
        "               else {\n"
        "                   gl_FragColor=vec4(1.0,1.0,1.0,1.0);\n"
        "               }\n"
        "           }\n"
        //outer     corner
        "           if(abs(sqrt(pow(pixToSides.w, 2.0)\n"
        "                        +pow(pixToSides.y, 2.0))-inner)<thick) {\n"
        "               if(state==4) {\n"
        "                   gl_FragColor=vec4(1.0,0.0,0.0,1.0);\n"
        "               }\n"
        "               else {\n"
        "                   gl_FragColor=vec4(1.0,1.0,1.0,1.0);\n"
        "               }\n"
        "           }\n"
        //inner     corner
        "           if(( abs(sqrt(pow(pixToSides.w, 2.0)\n"
        "                       +pow(pixToSides.y, 2.0))-inner*3.0)<thick)\n"
        "                   && pixToSides.x>inner && pixToSides.y>inner \n"
        "                   && pixToSides.z>inner && pixToSides.w>inner ){\n"
        "               if(state==2) {\n"
        "                   gl_FragColor=vec4(1.0,0.0,0.0,1.0);\n"
        "               }\n"
        "               else {\n"
        "                   gl_FragColor=vec4(1.0,1.0,1.0,1.0);\n"
        "               }\n"
        "           }\n"
        "       }\n"
        "   }\n"
        "   else {\n"
        "       if(pixToSides.x<thick ||  pixToSides.y<thick \n"
        "               || pixToSides.z<thick || pixToSides.w<thick) { \n"
        "           if(state>0) {\n"
        "               gl_FragColor=vec4(1.0,0.0,0.0,1.0);\n"
        "           }\n"
        "           else {\n"
        "               gl_FragColor=vec4(1.0,1.0,1.0,1.0);\n"
        "           }\n"
        "       }\n"
        "       else {\n"
        "           gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);\n"
        "       }\n"
        "   }\n"
        "}\n";
    glShaderSource(fragmentShader, 1, &fs_source, NULL);
    glCompileShader(fragmentShader);
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &compile_ok);
    if (!compile_ok) {
        cout << "Error in fragment shader" << endl;
        GLint maxLength = 0;
        glGetShaderiv(fragmentShader, GL_INFO_LOG_LENGTH, &maxLength);
        std::vector<GLchar> errorLog(maxLength);
        glGetShaderInfoLog(fragmentShader, maxLength, &maxLength, &errorLog[0]);
        vector<GLchar>::iterator itC=errorLog.begin();
        for(; itC!=errorLog.end(); ++itC) {
            cout<<*itC;
        }
        cout<<endl;
    }
    m_zoneProgram = glCreateProgram();
    glAttachShader(m_zoneProgram, vertexShader);
    glAttachShader(m_zoneProgram, fragmentShader);
    glLinkProgram(m_zoneProgram);
    glGetProgramiv(m_zoneProgram, GL_LINK_STATUS, &link_ok);
    if (!link_ok) {
        cout << "Error in glLinkProgram" << endl;
    }
    m_zoneWinSizeUniform = glGetUniformLocation(m_zoneProgram, "winSize");
    m_zoneSizeUniform = glGetUniformLocation(m_zoneProgram, "zoneSize");
    m_zonePosUniform = glGetUniformLocation(m_zoneProgram, "zonePos");
    m_cutSizeUniform = glGetUniformLocation(m_zoneProgram, "cutSize");
    m_cutPosUniform = glGetUniformLocation(m_zoneProgram, "cutPos");
    m_zoneMirrorUniform = glGetUniformLocation(m_zoneProgram, "mirrored");
    m_zoneStateUniform = glGetUniformLocation(m_zoneProgram, "state");
    m_zoneAlphaUniform = glGetUniformLocation(m_zoneProgram, "alpha");
    m_zoneInvertUniform = glGetUniformLocation(m_zoneProgram, "invert");
    m_zoneFilledUniform = glGetUniformLocation(m_zoneProgram, "filled");
    m_zoneWindowUniform = glGetUniformLocation(m_zoneProgram, "windowTexture");
    m_zoneCoordsUniform = glGetUniformLocation(m_zoneProgram, "coordsTexture");
    m_zoneInnerUniform = glGetUniformLocation(m_zoneProgram, "inner");
    m_zoneOuterUniform = glGetUniformLocation(m_zoneProgram, "outer");

    glActiveTexture(GL_TEXTURE0);
    glGenTextures(1, &m_firstTextureID);
    glBindTexture(GL_TEXTURE_2D, m_firstTextureID);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glActiveTexture(GL_TEXTURE1);
    glGenTextures(1, &m_secondTextureID);
    glBindTexture(GL_TEXTURE_2D, m_secondTextureID);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    }

    //GROUP PROGRAM
    {
    vertexShader = glCreateShader(GL_VERTEX_SHADER);
    const char *vs_source =
        "#version 110\n"
        "attribute vec3 coord;\n"
        "uniform vec2 winSize;\n"
        "uniform vec2 groupPos;\n"
        "uniform vec2 groupSize;\n"
        "uniform float mirrored;\n"
        "varying vec2 pixPos;\n"
        "void main(void) {\n"
        "   if(mirrored>0.0) {\n"
        "       pixPos = vec2(coord.x, coord.y);\n"
        "   }\n"
        "   else {\n"
        "       pixPos = vec2(coord.x, 1.0-coord.y);\n"
        "   }\n"
        "   vec2 pos = ((pixPos*groupSize+groupPos)/winSize)*2.0-1.0;\n"
        "   if(mirrored>0.0) {\n"
        "       gl_Position = vec4(pos.x, pos.y, 0.0, 1.0);\n"
        "   }\n"
        "   else {\n"
        "       gl_Position = vec4(pos.x, -pos.y, 0.0, 1.0);\n"
        "   }\n"
        "}\n";
    glShaderSource(vertexShader, 1, &vs_source, NULL);
    glCompileShader(vertexShader);
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &compile_ok);
    if (!compile_ok) {
        cout << "Error in vertex shader" << endl;
        GLint maxLength = 0;
        glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &maxLength);
        std::vector<GLchar> errorLog(maxLength);
        glGetShaderInfoLog(vertexShader, maxLength, &maxLength, &errorLog[0]);
        vector<GLchar>::iterator itC=errorLog.begin();
        for(; itC!=errorLog.end(); ++itC) {
            cout<<*itC;
        }
        cout<<endl;
    } 

    fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    const char *fs_source =
        "#version 110\n"
        "varying vec2 pixPos;\n"
        "uniform vec2 winSize;\n"
        "uniform float mirrored;\n"
        "uniform float editing;\n"
        "uniform float highlight;\n"
        "void main(void) {       \n"
        "   if(editing>0.0) {\n"
        "           gl_FragColor = vec4(0.0,1.0,0.0,1.0);\n"
        "   }\n"
        "   else {\n" 
        "       if(pixPos.x<0.01 ||  pixPos.x>0.99 \n"
        "                   || pixPos.y<0.01 || pixPos.y>0.99) { \n"
        "             gl_FragColor=vec4(0.0,0.5+highlight*0.5,0.0,1.0);\n"
        "       }\n"
        "       else {\n"
        "           gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);\n"
        "       }\n"
        "   }\n"
        "}\n";
    glShaderSource(fragmentShader, 1, &fs_source, NULL);
    glCompileShader(fragmentShader);
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &compile_ok);
    if (!compile_ok) {
        cout << "Error in fragment shader" << endl;
        GLint maxLength = 0;
        glGetShaderiv(fragmentShader, GL_INFO_LOG_LENGTH, &maxLength);
        std::vector<GLchar> errorLog(maxLength);
        glGetShaderInfoLog(fragmentShader, maxLength, &maxLength, &errorLog[0]);
        vector<GLchar>::iterator itC=errorLog.begin();
        for(; itC!=errorLog.end(); ++itC) {
            cout<<*itC;
        }
        cout<<endl;
    }
    m_groupProgram = glCreateProgram();
    glAttachShader(m_groupProgram, vertexShader);
    glAttachShader(m_groupProgram, fragmentShader);
    glLinkProgram(m_groupProgram);
    glGetProgramiv(m_groupProgram, GL_LINK_STATUS, &link_ok);
    if (!link_ok) {
        cout << "Error in glLinkProgram" << endl;
    }
    m_groupWinSizeUniform = glGetUniformLocation(m_groupProgram, "winSize");
    m_groupSizeUniform = glGetUniformLocation(m_groupProgram, "groupSize");
    m_groupPosUniform = glGetUniformLocation(m_groupProgram, "groupPos");
    m_groupMirrorUniform = glGetUniformLocation(m_groupProgram, "mirrored");
    m_groupEditingUniform = glGetUniformLocation(m_groupProgram, "editing");
    m_groupHighlightUniform = glGetUniformLocation(m_groupProgram, "highlight");
    }
    
    //CURSOR PROGRAM
    {
    vertexShader = glCreateShader(GL_VERTEX_SHADER);
    const char *vs_source =
        "#version 110\n"
        "attribute vec3 coord;\n"
        "uniform vec2 winSize;\n"
        "uniform vec2 cursPos;\n"
        "uniform float mirrored;\n"
        "varying vec2 pixPos;\n"
        "void main(void) {\n"
        "   if(mirrored>0.0) {\n"
        "       pixPos = vec2(coord.x, coord.y);\n"
        "   }\n"
        "   else {\n"
        "       pixPos = vec2(coord.x, 1.0-coord.y);\n"
        "   }\n"
        "   vec2 pos = ((pixPos*vec2(10,10)+cursPos)/winSize)*2.0-1.0;\n"
        "   if(mirrored>0.0) {\n"
        "       gl_Position = vec4(pos.x, pos.y, 0.0, 1.0);\n"
        "   }\n"
        "   else {\n"
        "       gl_Position = vec4(pos.x, -pos.y, 0.0, 1.0);\n"
        "   }\n"
        "}\n";
    glShaderSource(vertexShader, 1, &vs_source, NULL);
    glCompileShader(vertexShader);
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &compile_ok);
    if (!compile_ok) {
        cout << "Error in cursor vertex shader" << endl;
    } 

    fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    const char *fs_source =
        "#version 110\n"
        "uniform vec2 winSize;\n"
        "uniform vec2 cursPos;\n"
        "uniform float mirrored;\n"
        "varying vec2 pixPos;\n"
        "void main(void) {\n"
        "   if(pixPos.x<0.1 ||  pixPos.x>0.9 \n"
        "           || pixPos.y<0.1 || pixPos.y>0.9) { \n"
        "       gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);\n"
        "   }\n"
        "   else {\n"
        "       gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);\n"
        "   }\n"
        "}\n";
    glShaderSource(fragmentShader, 1, &fs_source, NULL);
    glCompileShader(fragmentShader);
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &compile_ok);
    if (!compile_ok) {
        cout << "Error in cursor fragment shader" << endl;
    }
    m_cursorProgram = glCreateProgram();
    glAttachShader(m_cursorProgram, vertexShader);
    glAttachShader(m_cursorProgram, fragmentShader);
    glLinkProgram(m_cursorProgram);
    glGetProgramiv(m_cursorProgram, GL_LINK_STATUS, &link_ok);
    if (!link_ok) {
        cout << "Error in cursor glLinkProgram" << endl;
    }
    m_cursorWinSizeUniform = glGetUniformLocation(m_cursorProgram, "winSize");
    m_cursorPosUniform = glGetUniformLocation(m_cursorProgram, "cursPos");
    m_cursorMirrorUniform = glGetUniformLocation(m_cursorProgram, "mirrored");
    }


    //DRAW PROGRAM
    {
    vertexShader = glCreateShader(GL_VERTEX_SHADER);
    const char *vs_source =
        "#version 110\n"
        "attribute vec3 coord;\n"
        "uniform vec2 winSize;\n"
        "uniform vec2 drawPos;\n"
        "uniform vec2 drawSize;\n"
        "uniform float mirrored;\n"
        "void main(void) {\n"
        "   vec2 pixPos;\n"
        "   if(mirrored>0.0) {\n"
        "       pixPos = vec2(coord.x, coord.y);\n"
        "   }\n"
        "   else {\n"
        "       pixPos = vec2(coord.x, 1.0-coord.y);\n"
        "   }\n"
        "   vec2 pos = ((pixPos*drawSize+drawPos)/winSize)*2.0-1.0;\n"
        "   if(mirrored>0.0) {\n"
        "       gl_Position = vec4(pos.x, pos.y, 0.0, 1.0);\n"
        "   }\n"
        "   else {\n"
        "       gl_Position = vec4(pos.x, -pos.y, 0.0, 1.0);\n"
        "   }\n"
        "}\n";
    glShaderSource(vertexShader, 1, &vs_source, NULL);
    glCompileShader(vertexShader);
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &compile_ok);
    if (!compile_ok) {
        cout << "Error in draw vertex shader" << endl;
    } 

    fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    const char *fs_source =
        "#version 110\n"
        "uniform vec2 winSize;\n"
        "uniform vec2 cursPos;\n"
        "uniform float mirrored;\n"
        "uniform float drawingGroup;\n"
        "void main(void) {\n"
        "   gl_FragColor = vec4(1.0-drawingGroup, drawingGroup, 0.0, 1.0);\n"
        "}\n";
    glShaderSource(fragmentShader, 1, &fs_source, NULL);
    glCompileShader(fragmentShader);
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &compile_ok);
    if (!compile_ok) {
        cout << "Error in draw fragment shader" << endl;
    }
    m_drawingProgram = glCreateProgram();
    glAttachShader(m_drawingProgram, vertexShader);
    glAttachShader(m_drawingProgram, fragmentShader);
    glLinkProgram(m_drawingProgram);
    glGetProgramiv(m_drawingProgram, GL_LINK_STATUS, &link_ok);
    if (!link_ok) {
        cout << "Error in cursor glLinkProgram" << endl;
    }
    m_drawWinSizeUniform = glGetUniformLocation(m_drawingProgram, "winSize");
    m_drawPosUniform = glGetUniformLocation(m_drawingProgram, "drawPos");
    m_drawSizeUniform = glGetUniformLocation(m_drawingProgram, "drawSize");
    m_drawMirrorUniform = glGetUniformLocation(m_drawingProgram, "mirrored");
    m_drawGroupUniform = glGetUniformLocation(m_drawingProgram, "drawingGroup");
    }

    //MENU PROGRAM
    {
    vertexShader = glCreateShader(GL_VERTEX_SHADER);
    const char *vs_source =
        "#version 110\n"
        "attribute vec3 coord;\n"
        "uniform vec2 winSize;\n"
        "uniform vec2 menuPos;\n"
        "uniform vec2 menuSize;\n"
        "uniform float mirrored;\n"
        "varying vec2 pixPos;\n"
        "void main(void) {\n"
        "   if(mirrored>0.0) {\n"
        "       pixPos = vec2(coord.x, coord.y);\n"
        "   }\n"
        "   else {\n"
        "       pixPos = vec2(coord.x, 1.0-coord.y);\n"
        "   }\n"
        "   vec2 pos = ((pixPos*menuSize+menuPos)/winSize)*2.0-1.0;\n"
        "   if(mirrored>0.0) {\n"
        "       gl_Position = vec4(pos.x, pos.y, 0.0, 1.0);\n"
        "   }\n"
        "   else {\n"
        "       gl_Position = vec4(pos.x, -pos.y, 0.0, 1.0);\n"
        "   }\n"
        "}\n";
    glShaderSource(vertexShader, 1, &vs_source, NULL);
    glCompileShader(vertexShader);
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &compile_ok);
    if (!compile_ok) {
        cout << "Error in menu vertex shader" << endl;
    } 

    fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    const char *fs_source =
        "#version 110\n"
        "varying vec2 pixPos;\n"
        "uniform float mirrored;\n"
        "uniform sampler2D menuTexture;\n"
        "void main(void) {\n"
        "   gl_FragColor = texture2D(menuTexture, pixPos);\n"
        "}\n";
    glShaderSource(fragmentShader, 1, &fs_source, NULL);
    glCompileShader(fragmentShader);
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &compile_ok);
    if (!compile_ok) {
        cout << "Error in menu fragment shader" << endl;
    }
    m_menuProgram = glCreateProgram();
    glAttachShader(m_menuProgram, vertexShader);
    glAttachShader(m_menuProgram, fragmentShader);
    glLinkProgram(m_menuProgram);
    glGetProgramiv(m_menuProgram, GL_LINK_STATUS, &link_ok);
    if (!link_ok) {
        cout << "Error in cursor glLinkProgram" << endl;
    }
    m_menuWinSizeUniform = glGetUniformLocation(m_menuProgram, "winSize");
    m_menuPosUniform = glGetUniformLocation(m_menuProgram, "menuPos");
    m_menuSizeUniform = glGetUniformLocation(m_menuProgram, "menuSize");
    m_menuMirrorUniform = glGetUniformLocation(m_menuProgram, "mirrored");
    m_menuTextureUniform = glGetUniformLocation(m_menuProgram, "menuTexture");
    }
    glActiveTexture(GL_TEXTURE0);
    glGenTextures(1, &m_firstTextureID);
    glBindTexture(GL_TEXTURE_2D, m_firstTextureID);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
}
#endif

void Controlar::draw() {



/*
    pthread_mutex_lock(&m_zonesMutex);
    if(m_askedScene>-1) {
        setScene(m_askedScene);
        m_askedScene=-1;
    }
    pthread_mutex_unlock(&m_zonesMutex);
*/
#ifdef GL
    if(!valid()) {
        initGL();
    }

    glViewport(0, 0, w(), h());
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

    //reset each window
    vector<CutWindow*>::iterator itWin = m_winsMan->editWindowList().begin();
    for(; itWin!=m_winsMan->editWindowList().end(); ++itWin) {
        (*itWin)->releaseImage();
    }

    //draw groups
    glUseProgram(m_groupProgram);
    glUniform1f(m_groupMirrorUniform, m_flipped);
    glUniform2f(m_groupWinSizeUniform, w(), h());
    map<unsigned int, GroupWidget*>::iterator itGr = m_groups.begin();
    for(; itGr!=m_groups.end(); ++itGr) {
        itGr->second->draw();
    }

    //draw zones
    m_currentWindowName="";
    glUseProgram(m_zoneProgram);
    glUniform1f(m_zoneMirrorUniform, m_flipped);
    glUniform2f(m_zoneWinSizeUniform, w(), h());
    glUniform1i(m_zoneWindowUniform, 0);
    glUniform1i(m_zoneCoordsUniform, 1);
    glUniform1f(m_zoneInnerUniform, ZoneWidget::INNER);
    glUniform1f(m_zoneOuterUniform, ZoneWidget::OUTER);
    map<unsigned int, ZoneWidget*>::iterator itZo = m_zones.begin();
    for(; itZo!=m_zones.end(); ++itZo) {
        itZo->second->drawZone(m_currentScene);
    }

    //draw menus 
    glUseProgram(m_menuProgram);
    glUniform1f(m_menuMirrorUniform, m_flipped);
    glUniform2f(m_menuWinSizeUniform, w(), h());
    glUniform1i(m_menuTextureUniform, 0);
    if(m_mainMenu->visible()) {
        m_mainMenu->draw();
    }
    else if(m_zoneMenu->visible()) {
        m_zoneMenu->draw();
    }
    if(m_mainPanel->visible()) {
        m_mainPanel->draw();
    }
    if(m_zonePanel->visible()) {
        m_zonePanel->draw();
    }
    //draw drawing
    if(m_drawing) {
        glUseProgram(m_drawingProgram);
        glUniform1f(m_drawMirrorUniform, m_flipped);
        glUniform2f(m_drawWinSizeUniform, w(), h());
        glUniform2f(m_drawSizeUniform, m_drawCurX-m_drawStartX, 
                                       m_drawCurY-m_drawStartY);
        glUniform2f(m_drawPosUniform, m_drawStartX, 
                                      m_drawStartY);
        glUniform1f(m_drawGroupUniform, m_drawingGroup);
        glDrawArrays(GL_TRIANGLES, 0, 6);
    }

//#ifndef LINUX
    //draw cursor
    glUseProgram(m_cursorProgram);
    glUniform1f(m_cursorMirrorUniform, m_flipped);
    glUniform2f(m_cursorWinSizeUniform, w(), h());
    glUniform2f(m_cursorPosUniform, Fl::event_x_root()-x(), 
                                    Fl::event_y_root()-y());
    glDrawArrays(GL_TRIANGLES, 3, 3);
//#endif

    glDisableVertexAttribArray(0);

#else 
    //redraw whole background only when needed
    if(damage()&FL_DAMAGE_ALL) {
        fl_draw_box(box(), 0, 0, w(), h(), FL_BLACK);
        map<unsigned int, ZoneWidget*>::iterator itZo 
            = m_zonesSets[m_currentSet].begin();
        for(; itZo!=m_zonesSets[m_currentSet].end(); ++itZo) {
            itZo->second->setRateReached();
        }
    }
    else { //redraw only dynamic zones background and mask prev curs
        fl_rect(m_cursPosX-5, 
                m_cursPosY-5, 
                10, 10, FL_BLACK);
        map<unsigned int, ZoneWidget*>::iterator itZo 
            = m_zonesSets[m_currentSet].begin();
        for(; itZo!=m_zonesSets[m_currentSet].end(); ++itZo) {
            if(itZo->second->getRate()==ZoneWidget::DYNAMIC) {
                ZoneWidget* wid = itZo->second;
                fl_rectf(wid->x(),
                         m_flipped?h()-wid->y()-wid->h():wid->y(),
                         wid->w(),
                         wid->h(),
                         FL_BLACK);
            }
        }
    }

    //reset windows
    vector<CutWindow*>::iterator itWin = m_winsMan->editWindowList().begin();
    for(; itWin!=m_winsMan->editWindowList().end(); ++itWin) {
        (*itWin)->releaseImage();
    }

    //draw groups
    map<unsigned int, GroupWidget*>::iterator itGr = m_groups.begin();
    for(; itGr!=m_groups.end(); ++itGr) {
        itGr->second->draw();
    }


    //draw zones
    map<unsigned int, ZoneWidget*>::iterator itZo 
        = m_zones.begin();
    for(; itZo!=m_zones.end(); ++itZo) {
        itZo->second->draw();
    }

    //draw menus 
    if(m_mainMenu->visible()) {
        m_mainMenu->draw();
    }
    else if(m_zoneMenu->visible()) {
        m_zoneMenu->draw();
    }
    if(m_mainBar->visible()) {
        m_mainBar->draw();
    }
    if(m_zoneBar->visible()) {
        m_zoneBar->draw();
    }
    //draw zone creation rectangles
    if(m_drawing) {
        fl_rectf(m_drawStartX, 
             m_flipped?h()-m_drawStartY-(m_drawCurY-m_drawStartY):m_drawStartY,
             m_drawCurX-m_drawStartX, 
             m_drawCurY-m_drawStartY, 
             m_drawingGroup?FL_GREEN:FL_RED);
    }

    //draw cursor replacement
    m_cursPosX = Fl::event_x_root()-x();
    m_cursPosY=m_flipped?h()-(Fl::event_y_root()-y()):Fl::event_y_root()-y();
    fl_rectf(m_cursPosX-5, 
             m_cursPosY-5, 
             10, 10, FL_BLACK);
    fl_rect(m_cursPosX-5, 
            m_cursPosY-5, 
            10, 10, FL_WHITE);
#endif

}

void Controlar::resize(int x, int y, int w, int h) {
    #ifdef GL
        Fl_Gl_Window::resize(x,y,w,h);
    #else 
        Fl_Double_Window::resize(x,y,w,h);
    #endif
}

CutWindow* Controlar::getWindow(const string& name) {
    return m_winsMan->getWindow(name);
}

CutWindow* Controlar::getWindow(const int& win) {
    return m_winsMan->editWindowList()[win];
}

void Controlar::cbOpen() {
    Fl_Native_File_Chooser fnfc;
    fnfc.title("Open ControlAR File");
    fnfc.type(Fl_Native_File_Chooser::BROWSE_FILE);
    if(fnfc.show()==0) {
        load(fnfc.filename());
    }
    m_mainMenu->hide();
}


void Controlar::cbSaveAs() {
    Fl_Native_File_Chooser fnfc;
    fnfc.title("Save ControlAR File");
    fnfc.type(Fl_Native_File_Chooser::BROWSE_SAVE_FILE);
    if(fnfc.show()==0) {
        save(fnfc.filename());
    }
    m_mainMenu->hide();
}

void Controlar::cbSave() {
    if(m_fileName.compare("")!=0) {
        save(m_fileName);
    }
    else {
        cbSaveAs();
    }
    m_mainMenu->hide();
}

void Controlar::load(const std::string& fileName) {

    DEBUG("Opening "<<fileName);
    bool open=true;
    xmlDocPtr doc = xmlReadFile(fileName.c_str(), NULL, 0);
    xmlNodePtr rootNode = xmlDocGetRootElement(doc);
    if(doc==NULL || rootNode==NULL) {
        DEBUG("Could not open file "<<fileName);
        open=false;
    }
    else if(xmlStrcmp(rootNode->name, (const xmlChar*)"ControllAR")) {
        xmlFreeDoc(doc);
        DEBUG("Could not open file "<<fileName);
        open=false;
    }
    if(open) { //if the file is valid
        m_fileName=fileName;

        //delete all widgets
        map<unsigned int, ZoneWidget*>::iterator itZo = m_zones.begin();
        for(; itZo!=m_zones.end(); ++itZo) {
            Fl::delete_widget(itZo->second);
        }
        m_zones.clear();
        m_scenes.clear();
        map<unsigned int, GroupWidget*>::iterator itGr = m_groups.begin();
        for(; itGr!=m_groups.end(); ++itGr) {
            Fl::delete_widget(itGr->second);
        }
        m_groups.clear();

        m_winsMan->updateWindowsList();

        //load/create all zone sets and retrieve midi parameters
        m_currentScene=0;
        xmlNodePtr sNode;
        for(sNode=rootNode->children; sNode; sNode=sNode->next) {
            if(sNode->type == XML_ELEMENT_NODE) {
                if(!xmlStrcmp(sNode->name, (const xmlChar *)"Group")) {
                    GroupWidget* wid = new GroupWidget();
                    addGroup(wid);
                    wid->load(sNode);
                }
                else if(!xmlStrcmp(sNode->name, (const xmlChar *)"Scene")) {
                    string sceneStr;
                    char* value = NULL;
                    value = (char*)xmlGetProp(sNode, (xmlChar*)"name");
                    if(value!=NULL) {
                        sceneStr=string(value);
                    }
                    m_scenes[m_currentScene]=sceneStr;
                    m_currentScene++;
                }
                else if (!xmlStrcmp(sNode->name, (const xmlChar *)"Zone")) {
                    ZoneWidget* wid = new ZoneWidget();
                    addZone(wid);
                    wid->load(sNode);
                }
                else if(!xmlStrcmp(sNode->name, (const xmlChar *)"Midi")) {
                    char* value = NULL;
                    value = (char*)xmlGetProp(sNode,(xmlChar*)"device");
                    if(value!=NULL) {
                        setMidiDeviceFromName(string(value));
                    }
                    value = (char*)xmlGetProp(sNode,(xmlChar*)"set_channel");
                    if(value!=NULL) {
                        m_midiChannel=atoi(value);
                    }
                    value = (char*)xmlGetProp(sNode,(xmlChar*)"set_type");
                    if(value!=NULL) {
                        m_midiType=atoi(value);
                    }
                    value = (char*)xmlGetProp(sNode,(xmlChar*)"set_control");
                    if(value!=NULL) {
                        m_midiControl=atoi(value);
                    }
                }
            }
        }

        //set current scene to 0
        m_currentScene=0;
        setScene(0);
        DEBUG("Opened file "<<fileName);
    }
    updateTitle();
    m_mainPanel->hide();
}

void Controlar::save(const std::string& fileName) {
    DEBUG("Saving to "<<fileName);
    xmlDocPtr doc = xmlNewDoc(BAD_CAST "1.0");
    xmlNodePtr rootNode = xmlNewNode(NULL, BAD_CAST "ControllAR");

    //save groups
    map<unsigned int, GroupWidget*>::iterator itGr = m_groups.begin();
    for(; itGr!=m_groups.end(); ++itGr) {
        itGr->second->save(rootNode);
    }

    //save scenes
    map<int, string>::iterator itSc = m_scenes.begin();
    for(; itSc!=m_scenes.end(); ++itSc) {
        xmlNewChild(rootNode, NULL, BAD_CAST "Scene", NULL);
    }

    //save zone
    map<unsigned int, ZoneWidget*>::iterator itZo = m_zones.begin();
    for(; itZo!=m_zones.end(); ++itZo) {
        itZo->second->save(rootNode);
    }

    //save midi 
    xmlNodePtr setNode = xmlNewChild(rootNode, NULL, 
                                     BAD_CAST "Midi", NULL);
    xmlNewProp(setNode, BAD_CAST "device", 
               BAD_CAST m_currentMidiDeviceName.c_str());
    ostringstream oss1, oss2, oss3;
    oss1<<m_midiChannel;
    xmlNewProp(setNode, BAD_CAST "set_channel", 
               BAD_CAST oss1.str().c_str());
    oss2<<m_midiType;
    xmlNewProp(setNode, BAD_CAST "set_type", 
               BAD_CAST oss2.str().c_str());
    oss3<<m_midiControl;
    xmlNewProp(setNode, BAD_CAST "set_control", 
               BAD_CAST oss3.str().c_str());


    xmlDocSetRootElement(doc, rootNode);
    xmlSaveFormatFileEnc(fileName.c_str(), doc, "UTF-8", 1);
    xmlFreeDoc(doc);
    xmlCleanupParser();
    m_fileName=fileName;
    updateTitle();
    m_mainPanel->hide();
}

void Controlar::addZone(ZoneWidget* zone) {
    unsigned int newID=0;
    while(m_zones.find(newID)!=m_zones.end()) {
        newID++;
    }
    zone->setID(newID);
    m_zones[newID] = zone;
    zone->callback(statZone, this);
    add(zone);
    m_zoneMenu->hide();
}

void Controlar::addGroup(GroupWidget* group) {
    unsigned int newID=0;
    while(m_groups.find(newID)!=m_groups.end()) {
        newID++;
    }
    group->setID(newID);
    m_groups[newID] = group;
    add(group);
}

void Controlar::moveAllZonesInsideGroupBy(GroupWidget* group, int dx, int dy) {
    map<unsigned int, ZoneWidget*>::iterator itZo = m_zones.begin();
    for(; itZo!=m_zones.end(); ++itZo) {
        if(itZo->second->x()>group->x() 
                && itZo->second->x()<group->x()+group->w() 
                && itZo->second->y()>group->y() 
                && itZo->second->y()<group->y()+group->h()) {
            itZo->second->position(itZo->second->x()+dx, 
                                   itZo->second->y()+dy);
        }
    }
}

void Controlar::cbZoneSelect(CutWindow* win) {
    m_clickedZone->setCutWin(win);
    m_zoneMenu->hide();
}

void Controlar::cbZoneClear() {
    m_clickedZone->setCutWin(NULL);
    m_zoneMenu->hide();
}

void Controlar::cbZoneDelete() {
    m_zones.erase(m_clickedZone->getID());
    Fl::delete_widget(m_clickedZone);
    m_zoneMenu->hide();
    m_zonePanel->hide();
}

void Controlar::cbZoneMove() {
    
}

void Controlar::cbZoneAlpha(const int& alpha) {
    m_clickedZone->setAlpha(alpha);
    m_zoneMenu->hide();
}

void Controlar::cbZoneTrans(const ZoneWidget::ZONE_TRANSFO& trans) {
    m_clickedZone->setTransformation(trans);
    m_zoneMenu->hide();
}

void Controlar::cbZoneShape(const ZoneWidget::ZONE_SHAPE& shape) {
    m_clickedZone->setShape(shape);
    m_zoneMenu->hide();
}

void Controlar::cbZoneMidiLearn() {
    m_clickedZone->learnMidi();
    m_zoneMenu->hide();
}

void Controlar::cbZoneMidiEffect(const ZoneWidget::ZONE_INPUT_EFFECT& effect) {
    m_clickedZone->setInputEffect(effect);
    m_zoneMenu->hide();
}

void Controlar::cbZoneCol(const ZoneWidget::ZONE_COLOR& col) {
    m_clickedZone->setColor(col);
    m_zoneMenu->hide();
}

void Controlar::cbZoneVisible(const ZoneWidget::ZONE_VISIBLE& vis) {
    if(vis==ZoneWidget::VISIBLE_CURRENT) {
        m_clickedZone->setVisible(m_currentScene);
    }
    else if(vis==ZoneWidget::VISIBLE_ALL) {
        m_clickedZone->setVisible();
    }
    m_zoneMenu->hide();
}

void Controlar::cbZoneUpdate(const ZoneWidget::ZONE_UPDATE& up) {
    if(up==ZoneWidget::UPDATE_CURRENT) {
        m_clickedZone->setUpdate(m_currentScene);
    }
    else if(up==ZoneWidget::UPDATE_ALL) {
        m_clickedZone->setUpdate();
    }
    m_zoneMenu->hide();
}

void Controlar::cbZoneContent(const ZoneWidget::ZONE_CONTENT& cont) {
    m_clickedZone->setContent(cont);
    m_zoneMenu->hide();
}

void Controlar::init() {
    this->show();
}

Controlar::~Controlar() {}

Controlar* Controlar::getInstance() {
    static Controlar instance;
    return &instance;
}

void Controlar::cbQuit() {
    remove(m_zonePanel);
    remove(m_mainPanel);
    exit(0);
}

void Controlar::cbMidiDev(const int& dev) {
    if(m_midiStream!=NULL) {
       Pm_Close(m_midiStream);
    }
    Pm_OpenInput(&m_midiStream, dev, NULL, 100, NULL, NULL);
    const PmDeviceInfo* info = Pm_GetDeviceInfo(dev);
    m_currentMidiDeviceName = string(info->name);
    m_mainMenu->hide();
    m_zoneMenu->hide();
}

void Controlar::setMidiDeviceFromName(const string& name) {
    for(unsigned int d=0; d<m_midiDeviceNames.size(); ++d) {
        if(m_midiDeviceNames[d].compare(name)==0) {
            cbMidiDev(m_midiDevices[d]);
        }
    }
}

void DetectionWindow::draw() {
    #ifdef OSX
    CGRect rect = CGRectMake(0,0,100,100);
    CGRect deviceRect = CGContextConvertRectToDeviceSpace(fl_gc, rect);
    Controlar::getInstance()->setDisplayScale(deviceRect.size.width
                                                / rect.size.width);
    #endif
    Fl::delete_widget(this);
}


int main(int argc, char* argv[]) {
    #ifndef GL
    Fl::visual(FL_DOUBLE|FL_RGB);
    #endif

    //first grab the display scale 
    DetectionWindow* detWin = new DetectionWindow();
    detWin->show();

    //then init controllar
    Controlar::getInstance()->init();
    fl_cursor(FL_CURSOR_NONE);
    return Fl::run();
}


