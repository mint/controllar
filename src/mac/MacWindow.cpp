/***************************************************************************
 *  MacWindow.cpp
 *  Part of 
 *  2016-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "MacWindow.hpp"
#include <iostream>

#include "../Controlar.hpp"

using namespace std;

MacWindow::MacWindow(CutWindowsManager* man, CGWindowID id): CutWindow(man) { 
    CGRect rect = CGRectMake(0, 0, 100, 100);
    m_idCArray[0] = id;
    m_idArray = CFArrayCreate(NULL, (const void **) m_idCArray, 1, NULL);
    CFArrayRef descArray = CGWindowListCreateDescriptionFromArray(m_idArray);
    if(CFArrayGetCount(descArray)>0) {
        CFDictionaryRef description = 
            (CFDictionaryRef) CFArrayGetValueAtIndex(descArray, 0);
        if (CFDictionaryContainsKey(description, kCGWindowBounds)) {
            CFDictionaryRef bounds = 
                    (CFDictionaryRef) CFDictionaryGetValue(description, 
                                                           kCGWindowBounds);
            if (bounds) {
                CGRectMakeWithDictionaryRepresentation(bounds, &rect);
            }
        }
    }
    CFRelease(descArray);

    m_posX = rect.origin.x; 
    m_posY = rect.origin.y; 
    m_width = rect.size.width;
    m_height = rect.size.height;
    m_offsetX = m_posX;
    m_offsetY = m_posY;
    m_pixPerRow = m_width;
    m_isBGR=true;
    m_needsOffset=true;
    m_pixPerRow=m_width;
    m_grabbed=false;
    m_defaultImgData = new uchar[m_width*m_height*3];
}

MacWindow::~MacWindow() {
    CFRelease(m_idArray);
}

int MacWindow::computeNbPixPerRow(const int& srcW, const int& srcH) {
    CGImageRef img = CGWindowListCreateImageFromArray(CGRectNull,
                                         m_idArray,
                                         kCGWindowImageBoundsIgnoreFraming);
         
    int nb = CGImageGetBytesPerRow(img)/4;
    m_width=CGImageGetWidth(img)*Controlar::getInstance()->getDisplayScale();
    m_height=CGImageGetHeight(img)*Controlar::getInstance()->getDisplayScale();
    CFRelease(img);
    return nb;
}

void MacWindow::getPixels(const int& srcX, const int& srcY, 
                          const int& srcW, const int& srcH, 
                          const vector<int>& srcIndices,
                          const vector<vector<int> >& srcCoords,
                          const vector<int>& destIndices,
                          const uchar& alpha,
                          const ZoneWidget::ZONE_COLOR& color,
                          uchar* destImg) {
    //CGRect rect = CGRectMake(m_posX+srcX, m_posY+srcY, srcW, srcH);
    //CGImageRef img = CGWindowListCreateImageFromArray(rect,
    CGImageRef img = CGWindowListCreateImageFromArray(CGRectNull,
                                             m_idArray,
                                             kCGWindowImageBoundsIgnoreFraming);
    if(img!=NULL) {
        CFDataRef data = CGDataProviderCopyData(CGImageGetDataProvider(img));
        uchar* buf = (uchar*)CFDataGetBytePtr(data);
        vector<int>::const_iterator itId = srcIndices.begin();
        vector<vector<int> >::const_iterator itCo = srcCoords.begin();
        vector<int>::const_iterator itDe = destIndices.begin();
        for(; itCo!=srcCoords.end(); ++itCo) {
            if((*itCo)[0]>=0) { 
                //copy rgb
                for(int c=0; c<3; ++c) {
                    destImg[(*itDe)] = (color==ZoneWidget::NORMAL)
                                            ?buf[(*itId)]
                                            :255-buf[(*itId)];
                    ++itId;
                    ++itDe;
                }
                //copy alpha
                destImg[(*itDe)] = alpha;
                ++itId;
                ++itDe;
            }
            else { //black
                for(int c=0; c<4; ++c) {
                    destImg[(*itDe)] = 0;
                    ++itId;
                    ++itDe;
                }
            }
        }
        //release CG stuff
        CFRelease(data);
        CFRelease(img);
    }
    else {
        cout<<"Error getting image of window "<<m_name<<endl;
    }
}

void MacWindow::releaseImage() {
    if(m_grabbed) {
        CFRelease(m_data);
        CFRelease(m_img);
        m_grabbed=false;
    }
}

uchar* MacWindow::grabImage() {
    if(!m_grabbed) {
        m_img = CGWindowListCreateImageFromArray(CGRectNull,
                                            m_idArray,
                                            kCGWindowImageBoundsIgnoreFraming);
        if(m_img) {
            m_data = CGDataProviderCopyData(CGImageGetDataProvider(m_img));
            m_imgData = (uchar*)CFDataGetBytePtr(m_data);
            m_grabbed=true;
        }
        else {
            m_imgData=m_defaultImgData;
        }
    }
    return m_imgData;
}

void MacWindow::storeImage(const int& id) {
    int size = m_pixPerRow*m_height*4;
    if(m_storedImgData.find(id)!=m_storedImgData.end()) {
        delete [] m_storedImgData[id];
    }
    m_storedImgData[id] = new uchar[size];
    memcpy(m_storedImgData[id], m_imgData, size);
}

uchar* MacWindow::retrieveImage(const int& id) {
    map<int, uchar*>::iterator itSt = m_storedImgData.find(id);
    if(itSt!=m_storedImgData.end()) {
        return itSt->second;
    }
    return NULL;
}


