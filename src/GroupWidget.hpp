/***************************************************************************
 *  GroupWidget.hpp
 *  Part of 
 *  2016-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GroupWidget_h
#define GroupWidget_h

#include <FL/Fl.H>
#include <FL/Fl_Widget.H>
#include <FL/fl_draw.H>
#include <GL/glew.h>
#include <FL/gl.h>

#include <libxml/parser.h>
#include <libxml/tree.h>

#include <sys/time.h>
#include <string>
#include <vector>

class GroupWidget : public Fl_Widget {

    public:
        GroupWidget(int x=0, int y=0, int w=10, int h=10);
        ~GroupWidget();
        void copy(GroupWidget*);
        void draw();
        int handle(int event);
        void save(xmlNodePtr parentNode);
        void load(xmlNodePtr);
        void resize(int x, int y, int w, int h);
        inline void setID(const unsigned int& id){m_id=id;}
        inline const unsigned int& getID(){return m_id;}
        void startDraggingGroup();
        void forceDraggingGroup();

    private:
        unsigned int m_id;
        bool m_dragging;
        bool m_highlight;
        bool m_editing;
        float m_dragX, m_dragY;
        float m_dragW, m_dragH;
        float m_dragStartX, m_dragStartY;
        float m_scaleX, m_scaleY;
    
#ifdef GL
        GLint m_sizeUniform;
        GLint m_posUniform;
        GLint m_editingUniform;
#endif
};


#endif

