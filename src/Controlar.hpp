/***************************************************************************
 *  Controlar.hpp
 *  Part of ControllAR
 *  2016-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef Controlar_h
#define Controlar_h

#include <string>
#include <map>
#include <vector>
#include <sys/time.h>

#ifdef GL
#include <FL/Fl_Gl_Window.H>
#else
#include <FL/Fl_Double_Window.H>
#endif

#include <FL/Fl_Tile.H>
#include <FL/Fl_Pack.H>
#include <FL/Fl_Scroll.H>
#include <FL/Fl_Hold_Browser.H>
#include <FL/Fl_Menu_Button.H>
#include <FL/Fl_Toggle_Button.H>
#include <FL/Fl_Native_File_Chooser.H>
#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Overlay_Window.H>

#include <portmidi.h>

#include "osc/ip/IpEndpointName.h"
#include "osc/ip/UdpSocket.h"
#include "osc/osc/OscPacketListener.h"

#include "FlipMenu.hpp"
#include "ZoneWidget.hpp"
#include "GroupWidget.hpp"
#include "CutWindowsManager.hpp"
class MainPanel;
class ZonePanel;

#define IP_MTU_SIZE 2048

#ifdef GL
class Controlar: public Fl_Gl_Window, public osc::OscPacketListener {
#else 
class Controlar: public Fl_Double_Window, public osc::OscPacketListener  {
#endif
    public:
        static Controlar* getInstance();
        ~Controlar();
        void init();
        void load(const std::string& fileName);
        void save(const std::string& fileName);

        void update();
        int handle(int event);
        void draw();
        void resize(int x, int y, int w, int h);
        CutWindow* getWindow(const std::string& name);
        inline int getNbWindows() {return m_winsMan->getNbWindows();}
        CutWindow* getWindow(const int&);
        void addZone(ZoneWidget*);
        void addGroup(GroupWidget*);
        void moveAllZonesInsideGroupBy(GroupWidget*, int dx, int dy);
        void refreshWindowList();

        //ZONE CALLBACKS
        static void statZone(Fl_Widget* w, void* f){ 
            Controlar *tmpf = static_cast<Controlar *>(f);
            tmpf->cbZone(w);
        }    
        void cbZone(Fl_Widget*);

        static void statZoneSelect(Fl_Widget* w, void* f){ 
            Controlar::getInstance()->cbZoneSelect(static_cast<CutWindow*>(f));
        }    
        void cbZoneSelect(CutWindow*);
        static void statZoneClear(Fl_Widget* w, void* f){ 
            Controlar::getInstance()->cbZoneClear();
        }    
        void cbZoneClear();
        static void statZoneDelete(Fl_Widget* w, void* f){ 
            Controlar::getInstance()->cbZoneDelete();
        }    
        void cbZoneDelete();
        static void statZoneMove(Fl_Widget* w, void* f){ 
            Controlar::getInstance()->cbZoneMove();
        }    
        void cbZoneMove();
        static void statZoneAlpha(Fl_Widget* w, void* f){ 
            Controlar::getInstance()->cbZoneAlpha(*((int*)f));
        }    
        void cbZoneAlpha(const int& alpha);
        static void statZoneShape(Fl_Widget* w, void* f){ 
            Controlar::getInstance()
                ->cbZoneShape(*((ZoneWidget::ZONE_SHAPE*)f));
        }    
        void cbZoneShape(const ZoneWidget::ZONE_SHAPE& shape);
        static void statZoneTrans(Fl_Widget* w, void* f){ 
            Controlar::getInstance()
                ->cbZoneTrans(*((ZoneWidget::ZONE_TRANSFO*)f));
        }    
        void cbZoneTrans(const ZoneWidget::ZONE_TRANSFO& trans);
        static void statZoneMidiLearn(Fl_Widget* w, void* f){ 
            Controlar::getInstance()->cbZoneMidiLearn();
        }    
        void cbZoneMidiLearn();
        static void statZoneMidiEffect(Fl_Widget* w, void* f){ 
            Controlar::getInstance()
                ->cbZoneMidiEffect(*((ZoneWidget::ZONE_INPUT_EFFECT*)f));
        }    
        void cbZoneMidiEffect(const ZoneWidget::ZONE_INPUT_EFFECT& trans);
        static void statZoneCol(Fl_Widget* w, void* f){ 
            Controlar::getInstance()->cbZoneCol(*((ZoneWidget::ZONE_COLOR*)f));
        }    
        void cbZoneCol(const ZoneWidget::ZONE_COLOR& col);
        static void statZoneVisible(Fl_Widget* w, void* f){ 
            Controlar::getInstance()
                        ->cbZoneVisible(*((ZoneWidget::ZONE_VISIBLE*)f));
        }    
        void cbZoneVisible(const ZoneWidget::ZONE_VISIBLE& vis);
        static void statZoneUpdate(Fl_Widget* w, void* f){ 
            Controlar::getInstance()
                        ->cbZoneUpdate(*((ZoneWidget::ZONE_UPDATE*)f));
        }    
        void cbZoneUpdate(const ZoneWidget::ZONE_UPDATE& up);
        static void statZoneContent(Fl_Widget* w, void* f){ 
            Controlar::getInstance()
                        ->cbZoneContent(*((ZoneWidget::ZONE_CONTENT*)f));
        }    
        void cbZoneContent(const ZoneWidget::ZONE_CONTENT& up);

        //MAIN CALLBACKS
        static void statOpen(Fl_Widget* w,void* f){ 
            Controlar *tmpf = static_cast<Controlar *>(f);
            tmpf->cbOpen();
        }    
        void cbOpen();

        static void statSave(Fl_Widget* w, void* f){ 
            Controlar::getInstance()->cbSave();
        }    
        void cbSave();
        static void statSaveAs(Fl_Widget* w, void* f){ 
            Controlar::getInstance()->cbSaveAs();
        }    
        void cbSaveAs();

        static void statSceneNext(Fl_Widget* w, void* f){ 
            Controlar::getInstance()->cbSceneNext();
        }    
        void cbSceneNext();
        static void statScenePrev(Fl_Widget* w, void* f){ 
            Controlar::getInstance()->cbScenePrev();
        }    
        void cbScenePrev();
/*
        static void statSetDup(Fl_Widget* w,void* f){ 
            Controlar::getInstance()->cbSetDuplicate();
        }    
        void cbSetDuplicate();
*/
        static void statSceneDel(Fl_Widget* w,void* f){ 
            Controlar::getInstance()->cbSceneDelete();
        }    
        void cbSceneDelete();
        static void statSceneLearn(Fl_Widget* w,void* f){ 
            Controlar::getInstance()->cbSceneLearn();
        }    
        void cbSceneLearn();
        void setScene(const int&);

        static void statFlip(Fl_Widget* w,void* f){ 
            Controlar::getInstance()->cbFlip();
        }    
        void cbFlip();
        inline bool isFlipped(){return m_flipped;}
        
        static void statFullscreen(Fl_Widget* w,void* f){ 
            Controlar::getInstance()->cbFullscreen();
        }    
        void cbFullscreen();
        static void statMidiDev(Fl_Widget* w,void* f){ 
            Controlar::getInstance()->cbMidiDev(*(int*)(f));
        }    
        void cbMidiDev(const int&);
        void setMidiDeviceFromName(const std::string&);

        static void statQuit(Fl_Widget* w,void* f){ 
            Controlar *tmpf = static_cast<Controlar *>(f);
            tmpf->cbQuit();
        }    
        void cbQuit();
        static void statZoneMenuWin(Fl_Widget* w, void* f){ 
            static_cast<ZoneWidget*>(w)->setCutWin(static_cast<CutWindow*>(f));
        }    

        void parseMidi();
        void openMidiDevice(const std::string& devName);
        void closeMidiDevice(const std::string& devName);
        void refreshMidiStreams();
        void detectMidiDevices();
        void updateTitle();

        ZoneWidget* startDuplicating(ZoneWidget* dupWid);

        virtual void ProcessMessage(const osc::ReceivedMessage& m,
                                    const IpEndpointName& remoteEndpoint);

        inline const int& getDisplayScale(){return m_displayScale;}
        inline void setDisplayScale(const int& s){m_displayScale=s;}

#ifdef GL
        void initGL();
        inline GLint getZoneSizeUniform(){return m_zoneSizeUniform;}
        inline GLint getZonePosUniform(){return m_zonePosUniform;}
        inline GLint getCutSizeUniform(){return m_cutSizeUniform;}
        inline GLint getCutPosUniform(){return m_cutPosUniform;}
        inline GLint getZoneStateUniform(){return m_zoneStateUniform;}
        inline GLint getZoneWindowUniform(){return m_zoneWindowUniform;}
        inline GLint getZoneCoordsUniform(){return m_zoneCoordsUniform;}
        inline GLint getZoneFilledUniform(){return m_zoneFilledUniform;}
        inline GLint getZoneAlphaUniform(){return m_zoneAlphaUniform;}
        inline GLint getZoneInvertUniform(){return m_zoneInvertUniform;}
        inline GLint getGroupSizeUniform(){return m_groupSizeUniform;}
        inline GLint getGroupPosUniform(){return m_groupPosUniform;}
        inline GLint getGroupEditingUniform(){return m_groupEditingUniform;}
        inline GLint getGroupHighlightUniform(){return m_groupHighlightUniform;}
        inline GLint getMenuSizeUniform(){return m_menuSizeUniform;}
        inline GLint getMenuPosUniform(){return m_menuPosUniform;}
        inline GLuint getFirstTextureID(){return m_firstTextureID;}
        inline GLuint getSecondTextureID(){return m_secondTextureID;}
        inline const std::string& getCurrentWindowName() { 
            return m_currentWindowName;
        }
        inline void setCurrentWindowName(const std::string& n) { 
            m_currentWindowName=n;
        }
#endif
        
    protected:  
        friend void* oscThreadFunction(void*);

    protected:
        UdpListeningReceiveSocket* m_socket;

    private:
        Controlar();
        std::string m_title;
        std::string m_fileName;
        int m_drawStartX, m_drawStartY;
        int m_drawCurX, m_drawCurY;
        bool m_drawing, m_drawingGroup;
        FlipMenu* m_zoneMenu;
        FlipMenu* m_mainMenu;
        MainPanel* m_mainPanel;
        ZonePanel* m_zonePanel;
        CutWindowsManager* m_winsMan;
        //std::vector<std::map<unsigned int, ZoneWidget*> > m_zonesSets;
        std::map<int, std::string> m_scenes;
        std::map<unsigned int, ZoneWidget*> m_zones;
        std::map<unsigned int, GroupWidget*> m_groups;
        int m_alphaLevels[4];
        int m_menuValues[10];
        //unsigned int m_currentSet;
        int m_currentScene;
        ZoneWidget* m_clickedZone;
        bool m_flipped;
        bool m_fullscreen;
        bool m_duplicating;
        ZoneWidget* m_duplicatedZone;
        PortMidiStream* m_midiStream;
        std::map<std::string, int> m_midiDevMap;
        std::map<std::string, PortMidiStream*> m_midiStreamsMap;
        std::vector<PortMidiStream*> m_midiStreamsVec;
        bool m_midiLearning;
        int m_midiType;
        int m_midiChannel;
        int m_midiControl;
        std::vector<int> m_midiDevices;
        std::vector<std::string> m_midiDeviceNames;
        std::string m_currentMidiDeviceName;
        pthread_t m_oscThread;
        pthread_mutex_t m_zonesMutex;
        int m_askedScene;
        int m_displayScale;

#ifdef GL
        GLuint m_vertexArrayID;
        GLuint m_vertexBuffer;
        GLfloat m_vertexBufferData[18];

        GLuint m_zoneProgram;
        GLint m_zoneWinSizeUniform;
        GLint m_zoneSizeUniform;
        GLint m_zonePosUniform;
        GLint m_cutSizeUniform;
        GLint m_cutPosUniform;
        GLint m_zoneMirrorUniform;
        GLint m_zoneStateUniform;
        GLint m_zoneFilledUniform;
        GLint m_zoneAlphaUniform;
        GLint m_zoneInvertUniform;
        GLint m_zoneWindowUniform;
        GLint m_zoneCoordsUniform;
        GLint m_zoneOuterUniform;
        GLint m_zoneInnerUniform;

        GLuint m_cursorProgram;
        GLint m_cursorWinSizeUniform;
        GLint m_cursorPosUniform;
        GLint m_cursorMirrorUniform;

        GLuint m_drawingProgram;
        GLint m_drawWinSizeUniform;
        GLint m_drawPosUniform;
        GLint m_drawSizeUniform;
        GLint m_drawMirrorUniform;
        GLint m_drawGroupUniform;

        GLuint m_menuProgram;
        GLint m_menuWinSizeUniform;
        GLint m_menuPosUniform;
        GLint m_menuSizeUniform;
        GLint m_menuMirrorUniform;
        GLint m_menuTextureUniform;
        GLuint m_firstTextureID;
        GLuint m_secondTextureID;
        std::string m_currentWindowName;
        
        GLuint m_groupProgram;
        GLint m_groupWinSizeUniform;
        GLint m_groupPosUniform;
        GLint m_groupSizeUniform;
        GLint m_groupMirrorUniform;
        GLint m_groupEditingUniform;
        GLint m_groupHighlightUniform;
#else
        int m_cursPosX;
        int m_cursPosY;
#endif
};

class DetectionWindow: public Fl_Double_Window {
    public:
        DetectionWindow():Fl_Double_Window(10, 10, ""){}
        void draw();
};

#endif

