/***************************************************************************
 *  MacWindow.hpp
 *  Part of 
 *  2016-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef MacWindow_h
#define MacWindow_h

#include <vector>
#include <string>

#include "../OvWindow.hpp"
#import <ApplicationServices/ApplicationServices.h>

class MacWindow: public OvWindow {
    public :
        MacWindow(OvWindowsManager* man, CGWindowID winID);
        virtual ~MacWindow();
        virtual void getPixels(const int& x, const int& y, 
                               const int& sx, const int& sy,
                               const std::vector<int>& srcIndices,
                               const std::vector<std::vector<int> >& srcCoords,
                               const std::vector<int>& destIndices,
                               const uchar& alpha,
                               const ZoneWidget::ZONE_COLOR& color,
                               uchar* destImg);
        int computeNbPixPerRow(const int& srcW, const int& srcH);

    protected:
        CGWindowID m_idCArray[1];
        CFArrayRef m_idArray;
};

#endif

