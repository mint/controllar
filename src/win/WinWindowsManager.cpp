/***************************************************************************
 *  MacWindowsManager.cpp
 *  Part of 
 *  2016-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*
    Relies on code from:
    https://github.com/JoelSjogren/window-copy/blob/master/windowutils.cpp
*/

#include "MacWindowsManager.hpp"
#include <iostream>
#include <stdlib.h>

using namespace std;

MacWindowsManager::MacWindowsManager(): OvWindowsManager() {

}

MacWindowsManager::~MacWindowsManager() {

}

void MacWindowsManager::updateWindowsList() {
    m_windowList.clear();
    CFArrayRef windowArray = 
      CGWindowListCreate(kCGWindowListExcludeDesktopElements
                         |kCGWindowListOptionOnScreenOnly, kCGNullWindowID);
    CFArrayRef descArray = CGWindowListCreateDescriptionFromArray(windowArray);
    if(descArray!=NULL) {
        for(int i=0; i<CFArrayGetCount(descArray); ++i) {
            CFDictionaryRef win = 
                (CFDictionaryRef)CFArrayGetValueAtIndex(descArray,i);
            CFStringRef name = 
                (CFStringRef) CFDictionaryGetValue(win, kCGWindowName);
            if(name!=NULL) {
                if(CFStringGetLength(name)>0) {
                    char tempStr[128];
                    CFStringGetCString(name,tempStr,128,kCFStringEncodingUTF8);
                    string nameStr(tempStr);
                    MacWindow* newWin = 
                        new MacWindow(this, 
                           (CGWindowID)(uintptr_t)
                                CFArrayGetValueAtIndex(windowArray,i));
                    newWin->setName(nameStr);
                    m_windowList.push_back(newWin);
                }
            }
            CFRelease(win);
        }
    }
    else {
        cout<<"Error: No windows found"<<endl;
    }
}


