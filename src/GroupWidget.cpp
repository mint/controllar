/***************************************************************************
 *  GroupWidget.cpp
 *  Part of 
 *  2016-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "GroupWidget.hpp"
#include <iostream>
#include <sstream>
#include <cmath>

#include "Controlar.hpp"

using namespace std;

GroupWidget::GroupWidget(int x, int y, int w, int h):Fl_Widget(x,y,w,h,"") {
    box(FL_FLAT_BOX);
    m_dragging=false;
    m_highlight=false;
    m_editing=false;
    m_scaleX=1;
    m_scaleY=1;
}

void GroupWidget::copy(GroupWidget* original) {
    resize(original->x(), original->y(), original->w(), original->h());
}

GroupWidget::~GroupWidget() {}

void GroupWidget::draw() {
    Controlar* cont = Controlar::getInstance();

    //draw
#ifdef GL
    glUniform1f(cont->getGroupHighlightUniform(), m_highlight);
    glUniform1f(cont->getGroupEditingUniform(), m_editing);
    glUniform2f(cont->getGroupSizeUniform(), w(), h());
    glUniform2f(cont->getGroupPosUniform(), x(), y());
    glDrawArrays(GL_TRIANGLES, 0, 6);
#else 
    //process drawing
    fl_rect(x(), cont->isFlipped()?cont->h()-y()-h():y(),w(),h(),FL_DARK_GREEN);
    if(m_highlight) {
        fl_rect(x(), cont->isFlipped()?cont->h()-y()-h():y(),w(),h(),FL_GREEN);
    }

#endif
}

int GroupWidget::handle(int event) {
    int res=0;
    switch(event) {
        case FL_PUSH:{
            switch(Fl::event_button()) {
                case FL_RIGHT_MOUSE: {
                    do_callback();
                    res=1;
                }break;
                case FL_LEFT_MOUSE: {
                    if(Fl::event_shift()) {
                        startDraggingGroup();
                        m_editing=true;
                        m_dragging=true;
                        res=1;
                    }
                }break;
                default:break;
            }
        }break;
        case FL_DRAG: {
            if(m_dragging) {
                if(Fl::event_button()==FL_LEFT_MOUSE) {
                    if(Fl::event_command()) { 
                        size(m_dragW+Fl::event_x(), m_dragH+Fl::event_y());
                    }
                    else {
                        Controlar* cont = Controlar::getInstance();
                        cont->moveAllZonesInsideGroupBy(this, 
                                        m_dragX+Fl::event_x()-x(),
                                        m_dragY+Fl::event_y()-y());
                        position(m_dragX+Fl::event_x(), 
                                 m_dragY+Fl::event_y());
                    }
                    res=1;
                }
            }
        }break;
        case FL_SHORTCUT: {
            if(m_highlight && Fl::event_key()==FL_Shift_L) {
                m_editing=true;
                res=1;
            }
        }break;
        case FL_KEYUP: {
            if(m_highlight && Fl::event_key()==FL_Shift_L) {
                m_editing=false;
                res=1;
            }
        }break;
        case FL_RELEASE: {
            if(m_dragging) {
                m_editing=false;
                m_dragging=false;
                res=1;
            }
        }break;
        case FL_MOVE:{
            m_highlight=true;
            if(Fl::event_shift()) { 
                m_editing=true;
                res=1;
            }
        }break;
        case FL_ENTER: {
            m_highlight=true;
            res=1;
        }break;
        case FL_LEAVE: {
            m_highlight=false;
            res=1;
        }break;
        default:break;
    }
    if(res==1) {
        parent()->redraw();
    }
    return res;
}

void GroupWidget::startDraggingGroup() {
    m_dragX=x()-Fl::event_x();
    m_dragY=y()-Fl::event_y();
    m_dragW=w()-Fl::event_x();
    m_dragH=h()-Fl::event_y();
}

void GroupWidget::resize(int nx, int ny, int nw, int nh) {
    Fl_Widget::resize(nx, ny, nw, nh);
}

void GroupWidget::load(xmlNodePtr groupNode) {
    char* value = NULL;

    int x=0,y=0,w=20,h=20;
    value = (char*)xmlGetProp(groupNode,(xmlChar*)"group_x");
    if(value!=NULL) {
        x = atoi(value);
    }
    value = (char*)xmlGetProp(groupNode,(xmlChar*)"group_y");
    if(value!=NULL) {
        y = atoi(value);
    }
    value = (char*)xmlGetProp(groupNode,(xmlChar*)"group_w");
    if(value!=NULL) {
        w = atoi(value);
    }
    value = (char*)xmlGetProp(groupNode,(xmlChar*)"group_h");
    if(value!=NULL) {
        h = atoi(value);
    }
    resize(x,y,w,h);
}

void GroupWidget::save(xmlNodePtr parentNode) {
    xmlNodePtr newNode = xmlNewChild(parentNode, NULL, 
                                      BAD_CAST "Group", NULL);
    ostringstream oss1, oss2, oss3, oss4, oss5, 
                  oss6, oss7, oss8, oss9, oss10, 
                  oss11, oss12, oss13, oss14, oss15,
                  oss16, oss17;
    oss5<<x();
    xmlNewProp(newNode, BAD_CAST "group_x", 
               BAD_CAST oss5.str().c_str());
    oss6<<y();
    xmlNewProp(newNode, BAD_CAST "group_y", 
               BAD_CAST oss6.str().c_str());
    oss7<<w();
    xmlNewProp(newNode, BAD_CAST "group_w", 
               BAD_CAST oss7.str().c_str());
    oss8<<h();
    xmlNewProp(newNode, BAD_CAST "group_h", 
               BAD_CAST oss8.str().c_str());
}


