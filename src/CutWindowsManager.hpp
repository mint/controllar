/***************************************************************************
 *  CutWindowsManager.hpp
 *  Part of Over 
 *  2013  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef CutWindowsManager_h
#define CutWindowsManager_h

#include <vector>
#include <string>
#include <iostream>

#include "CutWindow.hpp"

class CutWindowsManager {
    public :
        CutWindowsManager(){}
        virtual ~CutWindowsManager(){}

        virtual void updateWindowsList()=0;
        std::vector<CutWindow*>& editWindowList(){return m_windowList;}
        CutWindow* getWindow(const std::string& name) {
            CutWindow* res=NULL;
            std::vector<CutWindow*>::iterator itWin=m_windowList.begin();
            for(; itWin!=m_windowList.end(); ++itWin) {
                if((*itWin)->getName().compare(name)==0 && name.compare("")!=0){
                    res=(*itWin);
                }
            }
            return res;
        }
        int getNbWindows(){return m_windowList.size();}

    protected:
        std::vector<CutWindow*> m_windowList;
        std::map<unsigned int, CutWindow*> m_windowMap;
};

#endif

