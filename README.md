# ControllAR

## Description:

ControllAR is a system that facilitates the appropriation of rich visual feedback on control surfaces through remixing of graphical user interfaces and augmented reality display. A video example of the use of ControllAR can be seen on : https://vimeo.com/182821251

## Compiling/Running on GNU/Linux: 
* Install the following packages: 
 * g++ (or gcc-c++) 
 * libfltk-dev
 * libxml2-dev
 * libX11-dev
 * libxcb-dev
 * libportmidi
* Run the following commands from the controllar folder:
 * ./waf configure
 * ./waf
 * ./build/controllar
* To install : 
 * sudo ./waf install

## Compiling/Running on OSX: 
* Install MacPorts : http://www.macports.org/
* Run the following commands from a terminal :
 * port install gcc5
 * port install fltk
 * port install libxml2
 * port install portmidi
* Run the following commands from the controllar folder:
 * ./waf configure
 * ./waf
 * ./build/controllar (or open controllar.app in the build folder)

## Authors:
Florent Berthaut, Assistant Professor, Université de Lille, CRIStAL/MINT
florent_AT_hitmuri_DOT_net 

